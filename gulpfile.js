var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var testem = require('gulp-testem');
var jshint = require('gulp-jshint');
var http = require('http');
var fs = require('fs');

var mainBowerFiles = require('main-bower-files');

gulp.task('dist-plugins', function() {
    return gulp.src('src/plugins/**')
        .pipe(uglify({compress: { drop_console: true }}))
        .pipe(gulp.dest('./dist/plugins'));
});

gulp.task('dist', ['dist-plugins', 'jshint'], function() {
    var files = mainBowerFiles({includeDev: true});

    files.push('./src/hermit.js');
    files.push('./src/*.js');
    files.push('./src/operations/*.js');

    return gulp.src(files)
        .pipe(concat('hermit.js'))
        .pipe(gulp.dest('./dist'))
        .pipe(concat('hermit.min.js'))
        .pipe(uglify({compress: { drop_console: true }}))
        .pipe(gulp.dest('./dist'));

});

gulp.task('jshint', function() {
    return gulp.src(['./src/**/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter());
});

gulp.task('build', ['jshint'], function() {
    var files = mainBowerFiles({includeDev: true});

    files.push('./src/hermit.js');
    files.push('./src/*.js');
    files.push('./src/operations/*.js');

    return gulp.src(files)
        .pipe(concat('hermit.js'))
        .pipe(uglify({compress: false, mangle: false}))
        .pipe(gulp.dest('./build'));

});

gulp.task('coverage', ['build'], function () {
    console.log("Starting the coverage server");
    var coverageServer = http.createServer(function (req, resp) {
        req.pipe(fs.createWriteStream('./coverage.json'));
        resp.end();
    });

    var port = 7358;
    coverageServer.listen(7358, function(err){
        if(err)
            console.log("Unable to start coverage server", err);
        else
            console.log("Coverage Server Started on port", port);
    });
});

gulp.task('test', function () {
    gulp.src([''])
        .pipe(testem({
            configFile: 'testem-dev.json'
        }));
});

gulp.task('test-e2e', function () {
    gulp.src([''])
        .pipe(testem({
            configFile: 'testem-e2e.json'
        }));
});

gulp.task('test-coverage', ['coverage'], function () {
    gulp.src([''])
        .pipe(testem({
            configFile: 'testem-coverage.json'
        }));
});

gulp.task('test-release', ['dist'], function () {
    gulp.src([''])
        .pipe(testem({
            configFile: 'testem-rel.json'
        }));
});

gulp.task('auto', function() {
    return gulp.watch(['src/**/*.js'], ['test']);
});
