describe("DefaultModelProvider", function() {

    before(Hermit.reset);

    it('should be registered directly on the Hermit namespace', function(){
        expect(Hermit.DefaultModelProvider).not.to.be.null;
    });

    describe('DefaultModelProvider:getModel', function(){
        var dmp, hermit, dmp2;

        before(function() {
            hermit = new Hermit({id: 'hermit1'});
            dmp = new Hermit.DefaultModelProvider(hermit);
            dmp2 = new Hermit.DefaultModelProvider(hermit);
            dmp2.registerModel({
                key: 'model1'
            });
        });

        it('should return undefined if an unknown model key is provided', function() {
            var model = dmp.getModel('invalid');
            expect(model).to.be.undefined;
        });

        it('should return the right model for the specified key', function() {
            var model= dmp2.getModel('model1');
            expect(model).not.to.be.undefined;
            expect(model).to.be.an.instanceof(Hermit.Model);
            expect(model.key).to.equal('model1');
        });

        it('should append hermit id to model spec', function() {
            var model = dmp2.getModel('model1');
            expect(model.hermitId).to.equal('hermit1');
        });

        it('should call our visitor for each registered models', function(done){
            var visitor = sinon.spy(function(model) {
                expect(this.id).to.equal('hermit1');
                expect(model.key).to.equal('model1');
            });

            dmp2.visitModels(visitor).then(function(results){
                expect(results).to.be.instanceof(Array);
                expect(results.length).to.equal(1);
                expect(results[0]).to.equal('model1');
                done();
            });
        });
    });
});