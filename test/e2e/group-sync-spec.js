describe('Group Sync Spec', function() {
    var members = {};

    before(function() {
        members.h1 = new Hermit(_.merge(config, {id:'h1', group:'test-group'}));
        members.h2 = new Hermit(_.merge(config, {id:'h2', group:'test-group'}));
        members.h3 = new Hermit(_.merge(config, {id:'h3', group:'test-group'}));

        TestSetup.initializeModels(members.h1);
        TestSetup.initializeModels(members.h2);
        TestSetup.initializeModels(members.h3);

        members.h1.getNetworkStateManager().setNetworkConnected(true);
        members.h2.getNetworkStateManager().setNetworkConnected(true);
        members.h3.getNetworkStateManager().setNetworkConnected(true);
    });

    after(function() {
//        Hermit.reset();
    });

    describe('Realtime datastore update', function() {

        before(function(done){
            // Wait for all sync agents to be connected before performing the test
            Promise.all([
                members.h1.getConnectedSyncAgent(),
                members.h2.getConnectedSyncAgent(),
                members.h3.getConnectedSyncAgent()
            ]).then(function(){
                console.log("All sync agents are connected");
                done();
            });
        });

        afterEach(function(done) {
            this.timeout(30000);

            // Cleanup our inserted color
            members.h1.model('colors').remove('CO_E2E', { waitForConfirmation: true, timeout: 30000}).finally(done);
        });

        it('should insert new record in all datastores of the group', function(done) {
            this.timeout(30000);

            members.h1.model('colors').insert({
                key: 'CO_E2E',
                name: [{
                    locale: 'fr',
                    value: 'Couleur E2E'
                }, {
                    locale: 'en',
                    value: 'Color E2E'
                }]
            }, { waitForCommit: true, timeout: 30000 }).then(function(inserted) {
                expect(inserted[0].__temp).to.be.undefined;
                expect(inserted[0].__op).to.be.undefined;

                Promise.all([
                    members.h1.model('colors').ds,
                    members.h2.model('colors').ds,
                    members.h3.model('colors').ds
                ]).spread(function(ds1, ds2, ds3) {
                    expect(ds1.data.length).to.equal(1);
                    expect(ds1.data[0].key).to.equal('CO_E2E');
                    expect(ds1.data[0].__temp).to.be.undefined;
                    expect(ds2.data.length).to.equal(1);
                    expect(ds2.data[0].key).to.equal('CO_E2E');
                    expect(ds2.data[0].__temp).to.be.undefined;
                    expect(ds3.data.length).to.equal(1);
                    expect(ds3.data[0].key).to.equal('CO_E2E');
                    expect(ds3.data[0].__temp).to.be.undefined;
                }).catch(done).finally(done);

            });
        });

    });

    describe('Operation Propagation', function() {

    });

    describe('Operation Confirmation', function() {

    });

    describe('Operation Queueing', function(){

    });
});