describe('InitialLoadSpec', function() {
    var hermit;

    before(function() {
        hermit = new Hermit(config);

        TestSetup.initializeModels(hermit);
    });

    it('should reject the sync if the network is not available', function(done) {
        hermit.getNetworkStateManager().setNetworkConnected(false);
        hermit.sync().then(function() {
            expect(true).to.be.falsy;
        }).catch(function(err) {
            expect(err).to.eql({ error: true, code: 'no-network'});
        }).finally(done);
    });

    it('should preload all colors in local store', function(done) {
        this.timeout(10000);
        hermit.getNetworkStateManager().setNetworkConnected(true);
        hermit.sync().then(function() {
            hermit.model('colors').ds.then(function(ds) {
                expect(ds.data).to.be.instanceof(Array);
                expect(ds.data.length).to.equal(13);
                done();
            });
        }).catch(done);
    });

    describe('Offline Operations Tests', function() {

        before(function(done) {
            this.timeout(10000);
            hermit.getNetworkStateManager().setNetworkConnected(true);
            var promise = hermit.sync().then(function() {
                hermit.model('colors').ds.then(function(ds) {
                    expect(ds.data).to.be.instanceof(Array);
                    expect(ds.data.length).to.equal(13);
                });
            }).catch(done).finally(function() {
                // Shut down the network
                hermit.getNetworkStateManager().setNetworkConnected(false);
                done();
            });
        });

        after(function(){
            Hermit.reset();
        });

        it('should use local colors when network is off', function(done) {
            expect(hermit.getNetworkStateManager().isNetworkConnected()).to.be.falsy;

            hermit.model('colors').list().then(function(colors) {
                expect(colors).to.be.instanceof(Array);
                expect(colors.length).to.equal(13);
                done();
            }).catch(done);

        });

        it('should show a single color when the network is off', function(done) {
            expect(hermit.getNetworkStateManager().isNetworkConnected()).to.be.falsy;

            hermit.model('colors').show("CO_WTE", { keyField: 'key'}).then(function(color) {
                console.dir(color);
                expect(color).not.to.be.undefined;
                expect(color.key).to.equal('CO_WTE');
                done();
            }).catch(done);
        });

        it('should insert a new color locally and make sure the operation is pending', function(done){
            expect(hermit.getNetworkStateManager().isNetworkConnected()).to.be.falsy;

            hermit.model('colors').insert({key: 'CO_2E2_TEST', name: [{locale:'fr', value:'e2e-test'}]}).then(function(newColor) {
                expect(newColor).to.be.instanceof(Array);
                expect(newColor.length).to.equal(1);
                expect(newColor[0].__temp).to.equal(true);

                hermit.model('colors').ds.then(function(ds) {
                    expect(ds.data.length).to.equal(14);
                    done();
                });
            }).catch(done);

        });

        it('should update an existing color locally and make sure the operation is pending', function(done) {
            expect(hermit.getNetworkStateManager().isNetworkConnected()).to.be.falsy;

            hermit.model('colors').update('CO_WTE', { name: [{locale:'fr', value:'e2e-test'}]}).then(function(color) {
                expect(color.__temp).to.equal(true);
                expect(color.__op).not.to.be.undefined;

                hermit.model('colors').ds.then(function(ds) {
                    done();
                });

            }).catch(done);

        });

        it('should mark item as $removed', function(done) {
            expect(hermit.getNetworkStateManager().isNetworkConnected()).to.be.falsy;

            hermit.model('colors').remove('CO_WTE').then(function(color) {
                expect(color.__temp).to.equal(true);
                expect(color.__op).not.to.be.undefined;
                expect(color.__removed).to.equal(true);

                hermit.model('colors').ds.then(function(ds) {
                    done();
                });

            }).catch(done);

        });

    });

});
