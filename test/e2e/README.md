# End To End Tests

All E2E tests are performed using the openbeerdatabase (openbeerdatabase.com).

Please create a file called : credentials.json in the e2e folder and enter your
public and private token there. You can get a free public and private token by registering in the openbeerdatabase.
