
describe('DefaultCacheManager', function() {
    var hermit;

    before(function() {
        Hermit.reset();
        hermit = new Hermit();
    });

    describe('constructor', function() {

        it('should create a valid cache provider instance', function(){
            expect(Hermit.DefaultCacheProvider).not.to.be.undefined;
            var dcm = new Hermit.DefaultCacheProvider(hermit);
            expect(dcm.caches).not.to.be.undefined;
        });

    });

    describe('getCache', function() {
        var dcm;

        beforeEach(function(){
            dcm = new Hermit.DefaultCacheProvider(hermit);
        });

        it('should return an undefined cache for an undefined key', function() {
            var cache = dcm.getCache();
            expect(cache).to.be.undefined;
        });

        it('should return a valid instance for a new key', function() {
            var cache = dcm.getCache('my-cache');
            expect(cache).not.to.be.undefined;
        });

        it('should return the same cache for subsequent calls', function() {
            var cache = dcm.getCache('my-cache');
            expect(cache).not.to.be.undefined;
            var cache2 = dcm.getCache('my-cache');
            expect(cache2).to.eql(cache);
        });
    });

    describe('Cache', function() {
        var cache, dcm;

        beforeEach(function(){
            dcm = new Hermit.DefaultCacheProvider(hermit);
            cache = dcm.getCache('my-cache');
        });

        it('should return an undefined item if entry is not found', function(done) {
            cache.getEntry('undefined-key').then(function(entry) {
                expect(entry).to.be.undefined;
                done();
            });
        });

        it('should create a new entry',function() {
            var data = { field1: 'data'};
            expect(cache.entries).to.be.empty;
            cache.setEntry('entry-key', data);
            expect(cache.entries).to.have.property('entry-key');
            expect(cache.entries['entry-key'].ts).to.be.a('number');
            expect(cache.entries['entry-key'].data).to.eql(data);
            expect(cache.entries['entry-key'].watchdog).to.be.undefined;
        });

        it('should create an entry and expire it if an expression is provided', function(done) {
            var data = { field1: 'data'};
            cache.setEntry('entry-key-2', data, { expire: 10});
            setTimeout(function() {
                expect(cache.entries).to.have.property('entry-key-2');
            }, 5);
            setTimeout(function() {
                expect(cache.entries).to.be.empty;
                done();
            }, 15);
        });
    });


});
