
describe('DefaultNetworkStateManager', function() {
    var hermit, nsm;

    before(function() {
        hermit = new Hermit();
        nsm = hermit.getNetworkStateManager();
    });

    it('should start in network unavailable state', function() {
        expect(nsm.isNetworkConnected()).to.be.false;
    });

    it('network state should be true after setNetworkConnected call', function() {
        nsm.setNetworkConnected(true);
        expect(nsm.isNetworkConnected()).to.be.true;
    });

    it('network state should be false after setNetworkConnected call', function() {
        nsm.setNetworkConnected(false);
        expect(nsm.isNetworkConnected()).to.be.false;
    });

    it('network-state-changed event should be emitted', function() {
        sinon.stub(nsm, "emit");
        nsm.setNetworkConnected(true);
        expect(nsm.emit.calledWith('network-state-changed', {
            connected: true
        })).to.be.true;
    });
});
