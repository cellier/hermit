
describe('Model', function() {
    var hermit;

    before(function() {
        Hermit.reset();
        hermit = new Hermit({id:'model-test-instance'});
    });

    function cleanup(ds) {
        console.log("Received ds");
        if(_.isFunction(ds.list.restore)) {
            console.log("List: Restoring DS");
            ds.list.restore();
        }
        if(_.isFunction(ds.update.restore)) {
            console.log("Update: Restoring DS");
            ds.update.restore();
        }
        if(_.isFunction(ds.insert.restore)) {
            console.log("Insert: Restoring DS");
            ds.insert.restore();
        }
        if(_.isFunction(ds.upsert.restore)) {
            console.log("Upsert: Restoring DS");
            ds.upsert.restore();
        }
        if(_.isFunction(ds.remove.restore)) {
            console.log("Remove: Restoring DS");
            ds.remove.restore();
        }
    }

    describe('constructor', function() {
        it('should throw an exception if no key is provided', function() {
            try {
                new Hermit.Model({id: 'hermit1'});
            }
            catch(err) {
                expect(err).to.be.an.instanceof(Hermit.Errors.InvalidSpec);
            }

        });

        it('should apply provided spec key', function() {
            var model = new Hermit.Model({key: 'key'});
            expect(model.key).to.equal('key');
        });

        it('should produce a ds promise when a ds spec is provided', function(done) {
            var model = new Hermit.Model({key: 'model-key', ds: hermit.DS('model-key', { max: '10MB' })});
            expect(model.ds).to.be.an.instanceof(Promise);
            model.ds.then(function(ds) {
                expect(ds).to.be.an.instanceof(Hermit.DataStoreClass);
                done();
            });
        });

        it('should assign the device group to the device id if none is provided', function(){
            var model = new Hermit.Model({key: 'model-key', ds: hermit.DS('model-key', { max: '10MB' })});
            expect(model.group).to.equal('model-test-instance');
        });

    });

    describe('list', function() {
        var models = {}, hermit, ds;
        var data1 = [{id:1}];

        before(function() {
            hermit = new Hermit({id: 'hermit1'});

            ds = hermit.DS('localDSModel', { "max-size": '10MB' });

            models.networkOnly = hermit.getModelProvider().registerModel({
                key: 'networkOnly'
            });

            models.localDataStore = hermit.getModelProvider().registerModel({
                key: 'localDSModel',
                ds: ds
            });
        });

        beforeEach(function(done) {
            ds.then(function(ds1){
                sinon.spy(ds1, "list");
                done();
            });
        });

        afterEach(function(done) {
            ds.then(function(ds1) {
                if(_.isFunction(ds1.list.restore)) {
                    ds1.list.restore();
                }
                done();
            });
        });

        it('should produce an InvalidOperation as no network, local datastore or cache is available', function(done) {
            models.networkOnly.list().then(function() {
                console.log(arguments);
                done();
            }, function(err) {
                expect(err).to.be.an.instanceof(Hermit.Errors.OperationUnavailable);
                done();
            }).catch(function(err) {
                done(err);
            });
        });

        it('should delegate the list operation to the local datastore', function(done) {

            models.localDataStore.ds.then(function(ds) {

                // Perform the list on our model instance
                return models.localDataStore.list().then(function(result) {
                    console.log("Received list result", result);
                    expect(ds.list.callCount).to.equal(1);
                    done();
                });

            }).catch(done);

        });

    });

    describe('load', function() {
        var models = {}, hermit, ds;
        var data1 = [{id:1}];

        before(function() {
            hermit = new Hermit({id: 'hermit1'});
            ds = hermit.DS('localDSModel', { "max-size": '10MB' });
            models.networkOnly = hermit.getModelProvider().registerModel({
                key: 'networkOnly'
            });
            models.localDataStore = hermit.getModelProvider().registerModel({
                key: 'localDSModel',
                ds: ds
            });
        });

        beforeEach(function(done) {
            ds.then(function(ds1){
                sinon.spy(ds1, "insert");
                done();
            }).catch(done);
        });

        afterEach(function(done) {
            ds.then(function(ds1) {
                if(_.isFunction(ds1.insert.restore)) {
                    ds1.insert.restore();
                }
                done();
            }).catch(done);
        });

        it('should delegate the load operation to the local datastore', function(done) {

            models.localDataStore.ds.then(function(ds) {

                // Perform the load on our model instance
                return models.localDataStore.load([{key: 'id1', field1: 'preloaded', value: true}]).then(function(result) {
                    console.log("Received load result", result);
                    expect(ds.insert.callCount).to.equal(1);
                    done();
                });

            }).catch(done);

        });

    });

    describe('show', function() {
        var models = {}, hermit, ds;

        before(function() {
            hermit = new Hermit({id: 'hermit1'});
            ds = hermit.DS('localDSModel', { max: '10MB' });
            models.networkOnly = hermit.getModelProvider().registerModel({
                key: 'networkOnly'
            });
            models.localDataStore = hermit.getModelProvider().registerModel({
                key: 'localDSModel',
                ds: ds
            });
        });

        it('should produce an InvalidOperation as no network, local datastore or cache is available', function(done) {
            models.networkOnly.show().then(done, function(err) {
                expect(err).to.be.an.instanceof(Hermit.Errors.OperationUnavailable);
                done();
            }).catch(done);
        });

        it('should delegate the show operation to the local datastore', function(done) {

            models.localDataStore.ds.then(function(ds) {
                var data = {id:1};

                sinon.stub(ds, "show", function() {
                    return P.resolve(data);
                });

                // Perform the show on our model instance
                return models.localDataStore.show(1).then(function(result) {
                    console.log("Received show result", result);
                    expect(result).to.eql(data);
                    expect(ds.show.callCount).to.equal(1);
                    done();
                }).finally(function() {
                    ds.show.restore();
                });

            }).catch(done);

        });

    });

    describe('update', function() {
        var models = {}, hermit, ds, ds2;
        var data1 = [{id:1}];

        before(function(done) {
            hermit = new Hermit({id: 'hermit1'});

            ds = hermit.DS('localDSModel', { max: '10MB' });
            ds2 = hermit.DS('product', { max: '10MB' });

            ds2.then(function(ds1) {
                ds1.insert({id:1, existing: true, name: 'test-product'});
                done();
            }).catch(done);

            models.networkOnly = hermit.getModelProvider().registerModel({
                key: 'networkOnly'
            });

            models.localDataStore = hermit.getModelProvider().registerModel({
                key: 'localDSModel',
                ds: ds
            });

            models.products = hermit.getModelProvider().registerModel({
                key: 'products',
                ds: ds2
            });
        });


        afterEach(function(done) {
            Promise.all([
                ds.then(cleanup),
                ds2.then(cleanup)
            ]).then(function(){
                done();
            });

        });

        it('should produce an InvalidOperation as no network, local datastore or cache is available', function(done) {
            models.networkOnly.update().then(done, function(err) {
                console.log(err);
                expect(err).to.be.an.instanceof(Hermit.Errors.OperationUnavailable);
                done();
            }).catch(function(err){
                console.log(err);
                done();
            });
        });

        it('should delegate the update operation to the local datastore', function(done) {

            models.products.ds.then(function(ds) {
                var data = {id:1, newField: true};

                sinon.spy(ds, "update");

                // Perform the show on our model instance
                return models.products.update(1, data).then(function(result) {
                    console.log("Received update result", result);
                    expect(ds.update.callCount).to.equal(1);
                    done();
                });

            }).catch(done);
        });

        it('should update an existing model instance with the same id', function(done) {

            models.products.ds.then(function(ds) {
                var product = {id:1, inventory: 4};
                sinon.spy(ds, "update");

                // Perform the update on our model instance
                return models.products.update(product.id, product).then(function(result) {
                    console.log("Received update product", result);
                    expect(ds.update.callCount).to.equal(1);
                    expect(result.id).to.be.equal(product.id);
                    expect(result.existing).to.be.true;
                    expect(result.name).to.eql('test-product');
                    expect(result.inventory).to.equal(4);
                    expect(ds.data.length).to.equal(1);
                    done();
                });

            }).catch(done);

        });

        it('should not update if model is not defined, function(done)', function(done) {

            models.products.ds.then(function(ds) {
                var product = {id:2, inventory: 4};
                sinon.spy(ds, "update");

                // Perform the show on our model instance
                return models.products.update(product.id, product).then(function(){
                    expect(false).to.be.true;
                }).catch(function(result) {
                    console.log("Received update product", result);
                    expect(ds.update.callCount).to.equal(1);
                    expect(result).not.to.be.defined;
                    done();
                });
            });
        });


    });

    describe('insert', function() {
        var models = {}, ds;

        before(function() {
            ds = hermit.DS('localDSModel', { max: '10MB' });

            models.networkOnly = hermit.getModelProvider().registerModel({
                key: 'networkOnly'
            });

            models.localDataStore = hermit.getModelProvider().registerModel({
                key: 'localDSModel',
                ds: ds
            });

            hermit.getNetworkStateManager().setNetworkConnected(false);
        });

        after(function(done) {
            ds.then(function(ds) {
                console.log("Received ds");
                cleanup(ds);
                done();
            });
        });

        it('should delegate the insert operation to the local datastore', function(done) {

            models.localDataStore.ds.then(function(ds) {
                var data = {id:1, newField: true};

                sinon.spy(ds, "insert");

                console.log("Insert test");

                // Perform the show on our model instance
                return models.localDataStore.insert(data).then(function(result) {
                    console.log("Received insert result", result);
                    expect(ds.insert.callCount).to.equal(1);
                    expect(result[0].id).to.be.equal(data.id);
                    expect(result[0].newField).to.equal(true);
                    expect(result[0].__temp).to.equal(true);
                    done();
                });

            }).catch(done);

        });
    });

    describe('upsert', function() {
        var models = {}, hermit, ds, ds2;
        var data1 = [{id:1}];

        before(function(done) {
            hermit = new Hermit({id: 'hermit1'});

            ds = hermit.DS('localDSModel', { max: '10MB' });
            ds2 = hermit.DS('product', { max: '10MB' });

            ds2.then(function(ds) {
                ds.insert({id:1, existing: true, name: 'test-product'});
                done();
            }).catch(done);

            models.networkOnly = hermit.getModelProvider().registerModel({
                key: 'networkOnly'
            });

            models.localDataStore = hermit.getModelProvider().registerModel({
                key: 'localDSModel',
                ds: ds
            });

            models.products = hermit.getModelProvider().registerModel({
                key: 'products',
                ds: ds2
            });

        });

        afterEach(function(done) {
            Promise.all([
                ds.then(cleanup),
                ds2.then(cleanup)
            ]).then(function(){
                done();
            });

        });

        it('should delegate the upsert operation to the local datastore', function(done) {

            models.localDataStore.ds.then(function(ds) {
                var data = {id:1, newField: true};

                sinon.spy(ds, "upsert");

                // Perform the show on our model instance
                return models.localDataStore.upsert(1,data).then(function(result) {
                    console.log("Received upsert result", result);
                    expect(ds.upsert.callCount).to.equal(1);
                    expect(result.id).to.be.equal(data.id);
                    expect(result.newField).to.equal(true);
                    expect(result.__temp).to.equal(true);
                    done();
                });

            }).catch(done);

        });

        it('should update an existing model instance with the same id', function(done) {

            models.products.ds.then(function(ds) {
                var product = {id:1, inventory: 4};

                sinon.spy(ds, "upsert");

                // Perform the show on our model instance
                return models.products.upsert(product.id, product).then(function(result) {
                    console.log("Received upsert product", result);
                    expect(ds.upsert.callCount).to.equal(1);
                    expect(result.id).to.be.equal(product.id);
                    expect(result.existing).to.be.true;
                    expect(result.name).to.eql('test-product');
                    expect(result.inventory).to.equal(4);
                    expect(ds.data.length).to.equal(1);
                    done();
                });

            }).catch(done);

        });

        it('should insert a new model instance with a different id', function(done) {

            models.products.ds.then(function(ds) {
                var product = {id:2, inventory: 10, name: 'new-product'};

                sinon.spy(ds, "upsert");

                // Perform the upsert on our model instance
                return models.products.upsert(product.id, product).then(function(result) {
                    console.log("Received upsert product", result);
                    expect(ds.upsert.callCount).to.equal(1);
                    expect(result.id).to.be.equal(product.id);
                    expect(result.existing).to.be.undefined;
                    expect(result.name).to.eql('new-product');
                    expect(result.inventory).to.equal(10);
                    expect(ds.data.length).to.equal(2);
                    done();
                });

            }).catch(done);

        });

        it('should generate an id if only the key is provided', function(done) {

            models.products.ds.then(function(ds) {
                var product = {key:123, inventory: 10, name: 'new-product'};

                sinon.spy(ds, "upsert");

                // Perform the upsert on our model instance
                return models.products.upsert(product.key, product).then(function(result) {
                    console.log("Received upsert product", result);
                    expect(ds.upsert.callCount).to.equal(1);
                    expect(result.id).to.be.equal(product.key);
                    expect(result.existing).to.be.undefined;
                    expect(result.name).to.eql('new-product');
                    expect(result.inventory).to.equal(10);
                    expect(ds.data.length).to.equal(3);
                    done();
                });

            }).catch(done);

        });

    });


    describe('remove', function() {

        var models = {}, hermit, ds;

        before(function(done) {
            hermit = new Hermit({id: 'hermit1'});

            ds = hermit.DS('localDSModel', { max: '10MB' });

            models.networkOnly = hermit.getModelProvider().registerModel({
                key: 'networkOnly'
            });

            models.localDataStore = hermit.getModelProvider().registerModel({
                key: 'localDSModel',
                ds: ds
            });

            models.products = hermit.getModelProvider().registerModel({
                key: 'localDSModel',
                ds: ds
            });

            ds.then(function(ds) {
                console.log("Data store is initialized");
                return Promise.join(
                    ds.insert({id:1, existing: true, name: 'test-product-name'}),
                    ds.insert({id:2, existing: false, name: 'another product'})
                );
            }).catch(done).finally(done);

        });


        afterEach(function(done) {
            Promise.all([
                ds.then(cleanup)
            ]).then(function() {
                done();
            });

        });

        it('should remove an element', function(done) {

            models.products.ds.then(function(ds) {
                var data = {id:1, newField: true, anyField:'abcd'};

                sinon.spy(ds, "remove");

                // Perform the remove on our model instance
                return models.products.remove(data.id).then(function(result) {
                    console.log("Received remove result", result);
                    expect(ds.remove.callCount).to.equal(1);
                    expect(ds.data.length).to.equal(2);
                    expect(result.existing).to.equal(true);
                    expect(result.__removed).to.equal(true);
                    done();
                });
            }).catch(done);
        });

        it('should not remove an element if it does not exist', function(done) {

            models.products.ds.then(function(ds) {
                var data = {id:3, newField: true, anyField:'abcd'};

                sinon.spy(ds, "remove");

                // Perform the remove on our model instance
                return models.products.remove(data.id).then(function(result) {
                    console.log("Received remove result", result);
                    expect(result).to.be.undefined;
                    expect(ds.remove.callCount).to.equal(1);
                    expect(ds.data.length).to.equal(2);
                    done();
                });
            }).catch(done);
        });
    });
});
