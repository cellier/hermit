/**
 * Created by administrator on 14-10-08.
 */
/**
 * Created by administrator on 14-10-08.
 */

describe('Operation:remove', function() {
    var factory, hermit, ds;

    before(function(done) {
        Hermit.reset();
        hermit = new Hermit({id: 'op-factory-test'});
        factory = new Hermit.OperationFactory(hermit);
        ds = hermit.DS('test-local-ds', { max: '10MB' });

        ds.then(function(ds) {

            return Promise.join(
                ds.insert({key:1, existing: true, name: 'product1'}),
                ds.insert({key:2, existing: false, name: 'product2'})
            ).then(function(res){
                    done();
                });
        }).catch(done);
    });

    beforeEach(function() {
    });

    describe('Network enabled', function() {

        afterEach(function() {
        });

        it('should apply the changes to the local datastore', function(done) {
            var op = factory.newOperation({type: 'remove', model: { cache:true, ds:ds }, key: 1, data: {field: false, newField: "abcd" } });
            sinon.spy(op, "emit");
            op.execute().then(function() {
                expect(op.emit.args[4][0]).to.equal("sync-backend");
                done();
            }).catch(function(err){
                console.log(err);
                done(err);
            }).finally(function(){
                op.emit.restore();
            });
        });

        it('should invalidate the cache after data has been removed over the network', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(true);
            var op = factory.newOperation({type: 'remove', model: { key: '1', cache:true } });
            sinon.spy(hermit.getBackend(), "remove");
            sinon.spy(op, "emit");
            op.execute().then(function() {
                expect(hermit.getBackend().remove.called).to.be.true;
                expect(hermit.getBackend().remove.callCount).to.equal(1);
                expect(op.emit.getCall(2).args[1]).to.equal("invalidate-cache");
                done();
            }).catch(function(err){
                console.log(err);
                done(err);
            }).finally(function(){
                hermit.getBackend().remove.restore();
                op.emit.restore();
            });
        });

        it('should invalidate the cache after data has been removed over the network', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(true);
            var op = factory.newOperation({type: 'remove', model: { key: '3', cache:true } });
            sinon.spy(hermit.getBackend(), "remove");
            sinon.spy(op, "emit");
            op.execute().then(function() {
                expect(hermit.getBackend().remove.called).to.be.true;
                expect(hermit.getBackend().remove.callCount).to.equal(1);
                expect(op.emit.getCall(2).args[1]).to.equal("invalidate-cache");
                done();
            }).catch(function(err){
                console.log(err);
                done(err);
            }).finally(function(){
                hermit.getBackend().remove.restore();
                op.emit.restore();
            });
        });

        it('should not invalidate the cache and should record the operation to the journal if the network is down', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(false);
            var op = factory.newOperation({type: 'remove', model: { key: '1', cache:true } });
            sinon.spy(hermit.getBackend(), "remove");
            sinon.spy(op, "emit");
            op.execute().then(function() {
                expect(hermit.getBackend().remove.called).to.be.true;
                done();
            }).catch(function(err){
                console.log(err);
                done(err);
            }).finally(function(){
                hermit.getBackend().remove.restore();
                op.emit.restore();
            });
        });

    });
});
