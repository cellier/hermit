
describe('Operation:list', function() {
    var factory, hermit, ds;

    before(function() {
        Hermit.reset();
        hermit = new Hermit({id: 'op-factory-test'});
        factory = new Hermit.OperationFactory(hermit);
        ds = hermit.DS('test-local-ds', { max: '10MB' });
    });

    beforeEach(function() {
        hermit.getNetworkStateManager().setNetworkConnected(true);
    });

    describe('no-cache', function() {

        afterEach(function() {
            if(_.isFunction(hermit.getBackend().list.restore)) {
                hermit.getBackend().list.restore();
            }
        });

        it('should perform a call to backend if no local or cached data is present', function(done) {
            var op = factory.newOperation({type: 'list', model: { key: 'products' }});
            sinon.spy(hermit.getBackend(), "list");
            var promise = op.execute();
            console.dir(promise);
            promise.then(function() {
                expect(hermit.getBackend().list.called).to.be.true;
                expect(hermit.getBackend().list.callCount).to.equal(1);
                done();
            }).catch(done);
        });

        it('should throw an OperationUnavailable if network is not present', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(false);
            var op = factory.newOperation({type: 'list', model: { key: 'products' }});
            sinon.spy(hermit.getBackend(), "list");
            op.execute().catch(function(err) {
                expect(err).to.be.an.instanceof(Hermit.Errors.OperationUnavailable);
            }).finally(function(){
                hermit.getBackend().list.restore();
                done();
            });
        });

    });

    describe('cached', function() {
        var entries = [{
            id: 1,
            field: 'value',
            field2: true
        }];

        before(function() {
        });

        beforeEach(function() {
            var op = factory.newOperation({type: 'list', model: { key: 'my-model-key', cache: true }});
            hermit.getCache('my-model-key').setEntry(op.hash, entries);
        });

        afterEach(function(done) {
            ds.then(function(datastore) {
                if(_.isFunction(datastore.list.restore)) {
                    datastore.list.restore();
                }
                done();
            }).catch(done);
        });

        it('should retrieve elements from the cache even if network is down', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(false);
            var op = factory.newOperation({type: 'list', model: { key: 'my-model-key', cache: true }});
            op.execute().then(function(result) {
                console.log(op.uniqueKey());
                var cache = hermit.getCache('my-model-key');
                console.dir(cache);
                expect(result).to.eql(entries);
                done();
            }).catch(done);
        });

        it('should go directly to the backend if not found in cache and no local ds', function(done){
            var op = factory.newOperation({type: 'list', model: { key: 'my-model-key2', cache: true }});
            op.execute().then(function(result) {
                expect(result).to.eql([]);

                // Result should be automatically added to the cache
                expect(hermit.getCache('my-model-key2').entries[op.hash].data).to.eql([]);
                done();
            }).catch(done);
        });

        it('should go directly to the local ds if not found in cache', function(done) {
            ds.then(function(datastore) {
                sinon.spy(datastore, "list");
                var op = factory.newOperation({type: 'list', model: { key: 'my-model-key3', cache: true, ds: ds }});
                return op.execute().then(function(result) {
                    expect(result).to.eql([]);
                    expect(datastore.list.callCount).to.equal(1);
                    done();
                });
            }).catch(done);
        });

    });
});
