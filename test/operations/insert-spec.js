
describe('Operation:insert', function() {
    var factory, hermit, ds;

    before(function() {
        Hermit.reset();
        hermit = new Hermit({id: 'op-factory-test'});
        factory = new Hermit.OperationFactory(hermit);
        ds = hermit.DS('test-local-ds', { max: '10MB' });
    });

    beforeEach(function() {
    });

    describe('Network enabled', function() {

        afterEach(function() {
            if(_.isFunction(hermit.getBackend().insert.restore)) {
                hermit.getBackend().insert.restore();
            }
        });

        it('should invalidate the cache after data has been inserted over the network', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(true);
            var op = factory.newOperation({type: 'insert', model: { key: 'products', cache: true }, data: {field: true } });
            sinon.spy(hermit.getBackend(), "insert");
            sinon.spy(op, "emit");
            op.execute().then(function() {
                expect(hermit.getBackend().insert.called).to.be.true;
                expect(hermit.getBackend().insert.callCount).to.equal(1);
                expect(op.emit.args[5][0]).to.equal("invalidate-cache");
                done();
            }).catch(function(err){
                console.log(err);
                done(err);
            });
        });
    });

    describe('Network down', function() {

        afterEach(function() {
            if(_.isFunction(hermit.getBackend().insert.restore)) {
                hermit.getBackend().insert.restore();
            }
        });

        it('should not invalidate the cache and should record the operation to the journal if the network is down', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(false);
            var op = factory.newOperation({type: 'insert', model: { key: 'products', cache: true }, data: {field: true } });
            sinon.spy(hermit.getBackend(), "insert");
            sinon.spy(op, "emit");
            op.execute().then(function() {
                expect(hermit.getBackend().insert.called).to.be.true;
                done();
            }).catch(function(err){
                console.log(err);
                done(err);
            });
        });
    });

});
