/**
 * Created by administrator on 14-10-08.
 */

describe('Operation:show', function() {
    var factory, hermit, ds;

    before(function() {
        Hermit.reset();
        hermit = new Hermit({id: 'op-factory-test'});
        factory = new Hermit.OperationFactory(hermit);
        ds = hermit.DS('test-local-ds', { max: '10MB' });
    });

    beforeEach(function() {
        hermit.getNetworkStateManager().setNetworkConnected(true);
    });

    describe('no-cache', function() {

        afterEach(function(done) {
            done();
        });

        it('should perform a call to backend if no local or cached data is present', function(done) {
            var op = factory.newOperation({type: 'show', model: { key: 'products', cache:false}});
            sinon.spy(hermit.getBackend(), "show");
            op.execute().then(function() {
                expect(hermit.getBackend().show.called).to.be.true;
                expect(hermit.getBackend().show.callCount).to.equal(1);
                done();
            }).catch(function(err){
                console.log(err);
                done(err);
            }).finally(function(){
                hermit.getBackend().show.restore();
                op.emit.restore();
            });
        });

        it('should throw an OperationUnavailable if network is not present', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(false);
            var op = factory.newOperation({type: 'show', model: { key: 'products' }});
            sinon.spy(hermit.getBackend(), "show");
            op.execute().catch(function(err) {
                expect(err).to.be.an.instanceof(Hermit.Errors.OperationUnavailable);
            }).finally(function(){
                hermit.getBackend().show.restore();
                done();
            });
        });
    });


    describe('cached', function() {
        var entries = [{
            id: 1,
            field: 'value',
            field2: true
        }];

        beforeEach(function() {
            var op = factory.newOperation({type: 'show', model: { key: 'my-model-key', cache: true }});
            hermit.getCache('my-model-key').setEntry(op.hash, entries, { ikey: op.uniqueKey() });
        });

        afterEach(function(done) {
            done();
        });

        it('should retrieve elements from the cache even if network is down', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(false);
            var op = factory.newOperation({type: 'show', model: { key: 'my-model-key', cache: true }});
            op.execute().then(function(result) {
                expect(result).to.eql(entries);
                done();
            }).catch(function(err){
                console.log(err);
                done(err);
            }).finally(function(){
                hermit.getBackend().update.restore();
                op.emit.restore();
            });
        });

        it('should go directly to the backend if not found in cache and no local ds', function(done){
            hermit.getNetworkStateManager().setNetworkConnected(true);
            var op = factory.newOperation({type: 'show', model: { key: 'my-model-key2', cache: true }});
            op.execute().then(function(result) {
                console.dir(result);
                expect(result.key).to.be.undefined;
                //expect(result).to.eql([]);

                // Result should be automatically added to the cache
                console.dir(hermit.getCache('my-model-key2').entries[op.hash]);
                expect(hermit.getCache('my-model-key2').entries[op.hash].data.key).to.be.undefined;
                done();
            }).catch(function(err){
                console.log(err);
                done(err);
            });
        });

        it('should go directly to the local ds if not found in cache', function(done) {
            ds.then(function(datastore) {
                sinon.spy(datastore, "show");
                var op = factory.newOperation({type: 'show', model: { key: 'my-model-key3', cache: true, ds: ds }});
                return op.execute().then(function(result) {
                    expect(result).to.be.undefined;
                    expect(datastore.show.callCount).to.equal(1);
                    done();
                }).finally(function() {
                    datastore.show.restore();
                });
            }).catch(function(err){
                console.log(err);
                done(err);
            });
        });

    });
});
