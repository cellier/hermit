describe('Plugin:MockBackend', function() {
    var hermit, factory, backend;

    before(function() {
        Hermit.reset();
        hermit = new Hermit({id: 'mock-backend-test'});
        factory = new Hermit.OperationFactory(hermit);
    });

    beforeEach(function() {
        backend = new Hermit.MockBackend(hermit, {});
    });

    describe('update', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'update', model: { key: 'test-model', group: 'my-group'}, key: 'test-instance-1', data: { change: false }});
        });

        it('should invoke commit on the operation after a successful update', function(done) {
            sinon.spy(op, "emit");

            backend.execute(op).then(function() {
                expect(op.emit.getCall(0).args[0]).to.equal('commit');
                done();
            }).catch(done).finally(function() {
                op.emit.restore();
            });

        });

        it('should sync operation to all group members after a successful update', function(done) {
            sinon.spy(hermit.getSyncAgent(), "send");
            backend.execute(op).then(function() {
                expect(hermit.getSyncAgent().send.getCall(0).args[0]).to.eql([op.model.group, '*', op.model.key, 'sync']);
                done();
            }).catch(done).finally(function() {
                hermit.getSyncAgent().send.restore();
            });

        });

    });

    describe('insert', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'insert', model: { key: 'test-model', group: 'my-group'}, key: 'test-instance-1', data: { change: false }});
        });

        it('should invoke commit on the operation after a successful insert', function(done) {
            sinon.spy(op, "emit");

            backend.execute(op).then(function() {
                expect(op.emit.getCall(0).args[0]).to.equal('commit');
                done();
            }).catch(done).finally(function() {
                op.emit.restore();
            });

        });

        it('should sync operation to all group members after a successful insert', function(done) {
            sinon.spy(hermit.getSyncAgent(), "send");
            backend.execute(op).then(function() {
                expect(hermit.getSyncAgent().send.getCall(0).args[0]).to.eql([op.model.group, '*', op.model.key, 'sync']);
                done();
            }).catch(done).finally(function() {
                hermit.getSyncAgent().send.restore();
            });

        });

    });

    describe('upsert', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'upsert', model: { key: 'test-model', group: 'my-group'}, key: 'test-instance-1', data: { change: false }});
        });

        it('should invoke commit on the operation after a successful upsert', function(done) {
            sinon.spy(op, "emit");

            backend.execute(op).then(function() {
                expect(op.emit.getCall(0).args[0]).to.equal('commit');
                done();
            }).catch(done).finally(function() {
                op.emit.restore();
            });

        });

        it('should sync operation to all group members after a successful upsert', function(done) {
            sinon.spy(hermit.getSyncAgent(), "send");
            backend.execute(op).then(function() {
                expect(hermit.getSyncAgent().send.getCall(0).args[0]).to.eql([op.model.group, '*', op.model.key, 'sync']);
                done();
            }).catch(done).finally(function() {
                hermit.getSyncAgent().send.restore();
            });

        });

    });

    describe('remove', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'remove', model: { key: 'test-model', group: 'my-group'}, key: 'test-instance-1'});
        });

        it('should invoke commit on the operation after a successful remove', function(done) {
            sinon.spy(op, "emit");

            backend.execute(op).then(function() {
                expect(op.emit.getCall(0).args[0]).to.equal('commit');
                done();
            }).catch(done).finally(function() {
                op.emit.restore();
            });

        });

        it('should sync operation to all group members after a successful remove', function(done) {
            sinon.spy(hermit.getSyncAgent(), "send");
            backend.execute(op).then(function() {
                expect(hermit.getSyncAgent().send.getCall(0).args[0]).to.eql([op.model.group, '*', op.model.key, 'sync']);
                expect(hermit.getSyncAgent().send.getCall(0).args[1].length).to.eql(1);
                expect(hermit.getSyncAgent().send.getCall(0).args[1][0]).to.be.an.instanceof(Hermit.Operation);
                done();
            }).catch(done).finally(function() {
                hermit.getSyncAgent().send.restore();
            });

        });

    });

});
