describe('PouchDB_Journal', function(){
    var factory, hermit, ds, journal;

    before(function() {
        Hermit.reset();
        hermit = new Hermit({id: 'journal-test'});
        factory = new Hermit.OperationFactory(hermit);
        journal = new Hermit.PouchDBJournal(hermit, 'test-model-1');
    });

    beforeEach(function(done) {
        journal.clearQueue().then(function(){ done() });
    });

    it('should construct a valid journal instance', function() {
        expect(journal._hermit).to.eql(hermit);
        expect(journal.key).to.equal('test-model-1');
        expect(journal.db_name).to.equal('journal-test__test-model-1');
    });

    it('should add an operation to the queue', function(done){
        var promise = journal.enqueueOperation({id: 'op1', hash: 'op1-hash'});
        promise.then(function(op) {
            expect(op.ts).not.to.be.undefined;
            expect(op._id).to.equal(op.id);
            done();
        });
    });

    it('should keep a reference to an operation defer', function(done) {
        var defer = Promise.defer();
        var promise = journal.enqueueOperation({id: 'op1', hash: 'op1-hash', deferredResult: defer });
        promise.then(function(op) {
            expect(op.ts).not.to.be.undefined;
            expect(op._id).to.equal(op.id);
            expect(journal.activeOps['op1']).to.equal(defer);
            done();
        });
    });

});
