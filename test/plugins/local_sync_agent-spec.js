describe('Plugin:LocalSyncAgent', function() {
    var agent, hermit;

    before(function(){
        Hermit.reset();
        hermit = new Hermit({id: 'local-agent-sync-test-instance'});
    });

    beforeEach(function(){
        agent = new Hermit.LocalSyncAgent(hermit);
    });

    describe('constructor', function() {
        it('should create empty options if none is provided', function() {
            expect(agent.options).to.be.empty;
        });
    });

    describe('register', function() {

        it('should only receive insert operations when registering for inserts on a model', function(done) {
            var operation = {type:'insert'};
            var operation2 = {type:'update'};

            var callback = sinon.spy(function(op) {
                expect(op).to.be.eql(operation);
                expect(callback.callCount).to.equal(1);
                done();
            });

            agent.register({group:'test-group', model:'test-model', operations:'insert'}, callback);

            agent.send(['test-group', '*', 'test-model', 'insert'], operation);
            agent.send(['test-group', '*', 'test-model', 'update'], operation2);
        });

        it('should only receive operations for the given model', function(done) {
            var operation = {type:'insert'};

            var callback = sinon.spy(function(op) {
                expect(op).to.be.eql(operation);
                expect(callback.callCount).to.equal(1);
                done();
            });

            agent.register({group: 'test-group', model:'test-model-1', operations:'insert'}, callback);

            agent.send(['test-group', '*', 'test-model-1', 'insert'], operation);
            agent.send(['test-group', '*', 'test-model-2', 'insert'], operation);
        });

        it('should only receive operations for the right group', function(done) {
            var operation = {type:'insert'};

            var callback = sinon.spy(function(op) {
                expect(op).to.be.eql(operation);
                expect(callback.callCount).to.equal(1);
                done();
            });

            agent.register({group: 'test-group', model:'test-model-1', operations:'insert'}, callback);

            agent.send(['test-group', '*', 'test-model-1', 'insert'], operation);
            agent.send(['test-group-dummy', '*', 'test-model-1', 'insert'], operation);
        });

        it('should receives insert and update operations', function(done) {
            var operation = {type:'insert'};
            var operation2 = {type:'update'};

            var callback = sinon.spy(function(op) {
                expect(['insert', 'update']).to.include(op.type);
            });

            agent.register({group: 'test-group', model:'test-model-1', operations:['insert', 'update']} , callback);

            Promise.all([
                agent.send(['test-group', '*', 'test-model-1', 'insert'], operation),
                agent.send(['test-group', '*', 'test-model-1', 'update'], operation2),
                agent.send(['test-group', '*', 'test-model-1', 'remove'], operation2)
            ]).then(function() {
                console.log("All events were sent!");
                expect(callback.callCount).to.equal(2);
                done();
            });
        });

        it('should receives all operations on the right model', function(done) {
            var operation1 = {type:'insert'};
            var operation2 = {type:'update'};
            var operation3 = {type:'remove'};
            var operation4 = {type:'upsert'};
            var operation5 = {type:'list'};
            var operation6 = {type:'show'};

            var callback = sinon.spy(function(op) {
                expect(['insert', 'update', 'remove', 'upsert', 'list', 'show']).to.include(op.type);
            });

            agent.register({group:'test-group', model:'test-model-1'}, callback);

            Promise.all([
                agent.send(['test-group', '*', 'test-model-1', 'insert'], operation1),
                agent.send(['test-group', '*', 'test-model-1', 'update'], operation2),
                agent.send(['test-group', '*', 'test-model-1', 'remove'], operation3),
                agent.send(['test-group', '*', 'test-model-1', 'upsert'], operation4),
                agent.send(['test-group', '*', 'test-model-1', 'list'], operation5),
                agent.send(['test-group', '*', 'test-model-1', 'show'], operation6)
            ]).then(function() {
                console.log("All events were sent!");
                expect(callback.callCount).to.equal(6);
                done();
            }).catch(done);
        });

        it('should receives all operations independent of the provided model key', function(done) {
            var operation = {type:'insert', model: 'model1'};
            var operation2 = {type:'update', model: 'model2'};

            var callback = sinon.spy(function(op) {
                expect(['insert', 'update']).to.include(op.type);
                expect(['model1', 'model2']).to.include(op.model);
            });

            agent.register({group:'test-group', operations:['insert', 'update']}, callback);

            Promise.all([
                agent.send(['test-group', '*', 'test-model-1', 'insert'], operation),
                agent.send(['test-group', '*', 'test-model-2', 'update'], operation2),
                agent.send(['test-group', '*', 'test-model-1', 'remove'], operation2)
            ]).then(function() {
                console.log("All events were sent!");
                expect(callback.callCount).to.equal(2);
                done();
            });
        });

    });

});
