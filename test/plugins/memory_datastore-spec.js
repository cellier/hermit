describe("Plugin:MemoryDataStore", function(){

    var ds;
    before(function(){
        ds = new Hermit.MemoryDS('','products');
    });

    it('should create an id if only key is provided', function(done) {

        var data = {key:1, newField: true};

        sinon.spy(ds, "insert");

        // Perform the show on our model instance
        return ds.insert(data).then(function(result) {
            console.log("Received insert result", result);
            expect(ds.insert.callCount).to.equal(1);
            expect(result[0].id).to.be.equal(data.key);
            expect(ds.data.length).to.be.equal(1);
            done();
        });

    });
});
