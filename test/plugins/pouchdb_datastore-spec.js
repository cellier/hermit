function setup(ds, done) {
    // Insert test data
    var data = [
        { key: "1", name: "r1", test: true, field: 'value1'  },
        { key: "2", name: "r2", test: true, field: 'value2' },
        { key: "3", name: "r3", test: true, field: 'value3'  },
        { key: "4", name: "r4", test: true, field: 'value4'  },
        { key: "5", name: "r5", test: true, field: 'value5'  },
        { key: "6", name: "r6", test: true, field: 'value6'  },
        { key: "7", name: "r7", test: true, field: 'value7'  },
        { key: "8", name: "r8", test: true, field: 'value8'  },
        { key: "9", name: "r9", test: true, field: 'value9'  },
        { key: "10", name: "r10", test: true, field: 'value10'  },
        { key: "11", name: "r11", test: true, field: 'value11'  },
        { key: "12", name: "r12", test: true, field: 'value12'  },
        { key: "13", name: "r13", test: true, field: 'value13'  }
    ];

    var tempData = [
        {key: "T1", name: "T1", value: 'field1'},
        {key: "T2", name: "T2", value: 'field2'},
        {key: "T3", name: "T3", value: 'field3'},
        {key: "T4", name: "T4", value: 'field4'},
        {key: "T5", name: "T5", value: 'field5'},
        {key: "T6", name: "T6", value: 'field6'}
    ];

    // Add some committed and temp data
    return Promise.all([
        ds.insert(data, { commit: true }),
        ds.insert(tempData, { op: 'test-op-hash'})
    ]).then(function() {
        return ds.remove("13", {op: 'test-op-hash-2'});
    }).finally(function() {
        done();
    });
}

describe("Plugin:PouchDS", function() {

    describe('PouchDB:insert', function(){
        var ds;

        beforeEach(function() {
            ds = new Hermit.PouchDS('test-hermit1', 'products', { namespace: 'h_unit_test', idField: 'key'});
        });

        afterEach(function(done) {
            ds.reset().then(done);
            if(ds.insert.restore) {
                ds.insert.restore();
            }
        });

        it('should create an id if only key is provided', function(done) {
            var data = {key:"1", newField: true};
            sinon.spy(ds, "insert");

            // Perform the show on our model instance
            return ds.insert(data).then(function(result) {
                console.log("Received insert result", result);
                expect(ds.insert.callCount).to.equal(1);
                expect(result[0]._id).to.be.equal(data.key);
                expect(result.length).to.be.equal(1);
                done();
            }).catch(done);

        });

        it('should create temporary data if no commit option is provided', function(done) {
            var data = {key:"1", newField: true};
            sinon.spy(ds, "insert");
            return ds.insert(data, { op: "test-op-hash"}).then(function(result) {
                console.log("Received insert result", result);
                expect(ds.insert.callCount).to.equal(1);
                expect(result[0]._id).to.be.equal(data.key);
                expect(result.length).to.be.equal(1);
                expect(result[0].$temp).to.be.true;
                expect(result[0].$op).to.equal('test-op-hash');
                done();
            }).catch(done);

        });

        it('should throw an exception if no id is provided when inserting', function(done){
            var data = {new_key:"1", newField: true};
            sinon.spy(ds, "insert");
            return ds.insert(data).catch(function(err) {
                expect(err).not.to.be.undefined;
                expect(err).to.be.instanceof(Error);
                done();
            });
        });

        it('should handle inserting array of elements', function(done){
            var data = [{key:"1", newField: true}, {key: 2, newField: false, position: "second"}];
            sinon.spy(ds, "insert");

            // Perform the insert on our datastore
            return ds.insert(data, { op: "test-op-hash"}).then(function(result) {
                console.log("Received insert result", result);
                expect(ds.insert.callCount).to.equal(1);
                expect(result.length).to.be.equal(2);
                expect(result[0]._id).to.be.equal(data[0].key);
                expect(result[1]._id).to.be.equal(data[1].key.toString());
                expect(result[0].$temp).to.be.true;
                expect(result[1].$temp).to.be.true;
                expect(result[0].$op).to.equal('test-op-hash');
                expect(result[1].$op).to.equal('test-op-hash');
                done();
            }).catch(done);
        });

        it('should not mark new records as temp if the commit options is provided', function(done) {
            var data = [{key:"1", newField: true}, {key: 2, newField: false, position: "second"}];
            sinon.spy(ds, "insert");

            // Perform the insert on our datastore
            return ds.insert(data, { commit: true}).then(function(result) {
                console.log("Received insert result", result);
                expect(ds.insert.callCount).to.equal(1);
                expect(result.length).to.be.equal(2);
                expect(result[0]._id).to.be.equal(data[0].key);
                expect(result[1]._id).to.be.equal(data[1].key.toString());
                expect(result[0].$temp).to.be.undefined;
                expect(result[1].$temp).to.be.undefined;
                expect(result[0].$op).to.be.undefined;
                expect(result[1].$op).to.be.undefined;
                done();
            }).catch(done);
        });

    });

    describe('PouchDB:list', function() {
        var ds;

        beforeEach(function(done) {
            ds = new Hermit.PouchDS('test-hermit1', 'products', { namespace: 'h_unit_test', idField: 'key'});
            setup(ds, done);
        });

        afterEach(function(done) {
            ds.reset().then(done);
            if(ds.insert.restore) {
                ds.insert.restore();
            }
        });

        it('should return all committed records when list is called without options', function(done) {
            ds.list().then(function(records) {
                expect(records.data.length).to.equal(12);
                expect(records._pagination.total).to.equal(12);
                done();
            });
        });

        it('should include temporary records in result if includeTemp option is provided. removed fields should not be returned', function(done) {
            ds.list({includeTemp: true }).then(function(records) {
                expect(records.data.length).to.equal(18);
                expect(records._pagination.total).to.equal(18);
                done();
            });
        });

        it('should include all records if both includeRemoved and includeTemp are set', function(done) {
            ds.list({includeRemoved: true, includeTemp: true }).then(function(records) {
                expect(records.data.length).to.equal(19);
                expect(records._pagination.total).to.equal(19);
                done();
            });
        });

        it('should return the first 5 records if limit is set to 5', function(done) {
            ds.list({limit: 5, sortBy: function(doc) { return parseInt(doc.key) }, descending: false}).then(function(records) {
                expect(records.data.length).to.equal(5);
                expect(records._pagination.total).to.equal(5);
                expect(records.data[0].key).to.be.equal('1');
                expect(records.data[1].key).to.be.equal('2');
                expect(records.data[2].key).to.be.equal('3');
                expect(records.data[3].key).to.be.equal('4');
                expect(records.data[4].key).to.be.equal('5');
                done();
            });
        });

        it('should return records 6 to 10', function(done) {
            ds.list({skip: 5, limit: 5, sortBy: function(doc) { return parseInt(doc.key) }, descending: false}).then(function(records) {
                expect(records.data.length).to.equal(5);
                expect(records._pagination.total).to.equal(5);
                expect(records.data[0].key).to.be.equal('6');
                expect(records.data[1].key).to.be.equal('7');
                expect(records.data[2].key).to.be.equal('8');
                expect(records.data[3].key).to.be.equal('9');
                expect(records.data[4].key).to.be.equal('10');
                done();
            });
        });

        it('should return records 7, 8, 9', function(done) {
            ds.list({query: { field: { $in: ['value7', 'value8', 'value9']}}}).then(function(records) {
                expect(records.data.length).to.equal(3);
                expect(records.data[0].key).to.be.equal('7');
                expect(records.data[1].key).to.be.equal('8');
                expect(records.data[2].key).to.be.equal('9');
                done();
            });
        });

        it('should not return temp records in query', function(done) {
            ds.list({query: { value: 'field3' } }).then(function(records) {
                expect(records.data.length).to.equal(0);
                done();
            });
        });

        it('should return temp records in query if includeTemp is provided', function(done) {
            ds.list({query: { value: 'field3' }, includeTemp: true }).then(function(records) {
                expect(records.data.length).to.equal(1);
                expect(records.data[0].key).to.be.equal('T3');
                done();
            });
        });

    });

    describe('PouchDB:update', function() {
        var ds;

        beforeEach(function(done) {
            ds = new Hermit.PouchDS('test-hermit1', 'products', { namespace: 'h_unit_test', idField: 'key'});
            setup(ds, done);
        });

        afterEach(function(done) {
            ds.reset().then(done);
            if(ds.insert.restore) {
                ds.insert.restore();
            }
        });

        it('should modify the field value of the right record', function(done) {
            ds.update('2', { value: 'field_2'}, { op: 'test-op-hash'}).then(function(obj) {
                expect(obj.key).to.equal('2');
                expect(obj.test).to.be.true;
                expect(obj.$temp).to.be.true;
                expect(obj.$op).to.equal('test-op-hash');
                done();
            });
        });

        it('should produce an error if key isnt found', function(done) {
            ds.update('22', { value: 'field_2'}, { op: 'test-op-hash'}).catch(function(err) {
                expect(err).not.to.be.undefined;
                expect(err.error).to.be.true;
                expect(err.name).to.equal('not_found');
                expect(err.status).to.equal(404);
                done();
            });
        });

    });

    describe('PouchDB:upsert', function() {
        var ds;

        beforeEach(function(done) {
            ds = new Hermit.PouchDS('test-hermit1', 'products', { namespace: 'h_unit_test', idField: 'key'});
            setup(ds, done);
        });

        afterEach(function(done) {
            ds.reset().then(done);
            if(ds.insert.restore) {
                ds.insert.restore();
            }
        });

        it('should modify the field value of an existing record', function(done) {
            ds.upsert('2', { value: 'field_2'}, { op: 'test-op-hash'}).then(function(obj) {
                expect(obj.key).to.equal('2');
                expect(obj.test).to.be.true;
                expect(obj.$temp).to.be.true;
                expect(obj.$op).to.equal('test-op-hash');
                done();
            });
        });

        it('should create the new record if key isnt found', function(done) {
            ds.upsert('22', { value: 'field_2', test: true, key: '22'}, { op: 'test-op-hash'}).then(function(obj) {
                expect(obj.key).to.equal('22');
                expect(obj.test).to.be.true;
                expect(obj.value).to.equal('field_2');
                expect(obj.$temp).to.be.true;
                expect(obj.$op).to.equal('test-op-hash');
                done();
            });
        });


    });

    describe('PouchDB:remove', function() {
        var ds;

        beforeEach(function(done) {
            ds = new Hermit.PouchDS('test-hermit1', 'products', { namespace: 'h_unit_test', idField: 'key'});
            setup(ds, done);
        });

        afterEach(function(done) {
            ds.reset().then(done);
            if(ds.insert.restore) {
                ds.insert.restore();
            }
        });

    });

    describe('PouchDB:show', function() {
        var ds;

        beforeEach(function(done) {
            ds = new Hermit.PouchDS('test-hermit1', 'products', { namespace: 'h_unit_test', idField: 'key'});
            setup(ds, done);
        });

        afterEach(function(done) {
            ds.reset().then(done);
            if(ds.insert.restore) {
                ds.insert.restore();
            }
        });
    });

    describe('PouchDB:reset', function() {
        var ds;

        beforeEach(function(done) {
            ds = new Hermit.PouchDS('test-hermit1', 'products', { namespace: 'h_unit_test', idField: 'key'});
            setup(ds, done);
        });

        afterEach(function(done) {
            ds.reset().then(done);
            if(ds.insert.restore) {
                ds.insert.restore();
            }
        });
    });

    describe('PouchDB:synced', function() {

    });

    describe('PouchDB:confirm', function() {
        var ds;

        beforeEach(function(done) {
            ds = new Hermit.PouchDS('test-hermit1', 'products', { namespace: 'h_unit_test', idField: 'key'});
            setup(ds, done);
        });

        afterEach(function(done) {
            ds.reset().then(done);
            if(ds.insert.restore) {
                ds.insert.restore();
            }
        });

        it('should remove $temp from the record', function(done) {
            ds.confirm('test-op-hash').then(function(result) {
                expect(result.confirmed).to.equal(6);
                expect(result.removed).to.equal(0);
                done();
            }).catch(done);
        });

        it('should remove a $removed record', function(done) {
            ds.confirm('test-op-hash-2').then(function(result){
                expect(result.confirmed).to.equal(1);
                expect(result.removed).to.equal(1);
                done();
            }).catch(done);
        });
    });
});
