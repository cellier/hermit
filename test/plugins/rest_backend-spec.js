describe('Plugin:RestBackend', function() {
    var hermit, factory, backend;

    before(function() {
        Hermit.reset();
        hermit = new Hermit({id: 'rest-backend-test'});
        factory = new Hermit.OperationFactory(hermit);
    });

    beforeEach(function() {
        backend = new Hermit.RestBackend(hermit, {baseUrl : "http://localhost:8000"});
    });

    it('should throw an invalidOperation exception if operation type is unsupported', function(done) {
        var op = factory.newOperation({ type: 'list', model: { key: 'test-model', group: 'my-group'} });
        op.type = 'test';
        try {
            backend.execute(op);
        }
        catch(err) {
            expect(err).to.be.instanceof(Hermit.Errors.InvalidOperation);
            done();
        }
    });

    it('should throw an InvalidConfiguration exception if operation has no model', function(done){
        var op = factory.newOperation({ type: 'list'});
        try {
            backend.execute(op);
        }
        catch(err) {
            expect(err).to.be.instanceof(Hermit.Errors.InvalidConfiguration);
            done();
        }
    });

    it('should apply any normalize method specific to the backend', function(done){
        var hermitCfg = new Hermit({id:'normalize_hermit_with_cfg', backend_cfg: {
            normalize: function(resp, op) {
                return {
                    resp: resp,
                    op: op,
                    normalized: true
                }
            }
        }});
        hermitCfg.getNetworkStateManager().setNetworkConnected(true);
        var f = new Hermit.OperationFactory(hermitCfg);
        var op = f.newOperation({ type: 'list', model: { key: 'test-model', backend_cfg: { key: 'test-backend-model' } } });
        var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

        sinon.stub(window, "ajax", function(url, opts) {
            return opts.success("[]", {});
        });
        rest.execute(op).then(function(normalizedResult) {
            expect(normalizedResult).not.to.be.undefined;
            expect(normalizedResult.resp).to.eql([]);
            expect(normalizedResult.op.id).to.eql(op.id);
            expect(normalizedResult.normalized).to.be.true;
        }).finally(function() {
            if(window.ajax.restore)
                window.ajax.restore();
            done();
        });

    });

    it('should reject operation when an HTTP error is encountered', function(done){
        hermit.getNetworkStateManager().setNetworkConnected(true);
        var op = factory.newOperation({ type: 'list', model: { key: 'test-model', group: 'my-group'} });

        sinon.stub(window, "ajax", function(url, opts) {
            return opts.error({status: 500, statusText: 'test-error'});
        });

        backend.execute(op).catch(function(err) {
            expect(err).to.eql({status: 500, msg: "test-error"});
        }).finally(function() {
            window.ajax.restore();
            done();
        });
    });

    it('should using JSONP if required by the backend', function(done) {
        var hermitCfg = new Hermit({id:'list_hermit_with_cfg', backend_cfg: {
            cors:{
                jsonp: true
            }
        }});
        hermitCfg.getNetworkStateManager().setNetworkConnected(true);
        var f = new Hermit.OperationFactory(hermitCfg);
        var op = f.newOperation({ type: 'list', model: { key: 'test-model', backend_cfg: { key: 'test-backend-model' } } });
        var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

        sinon.stub(JSONP, "get", function(url, data, callback) {
            callback([]);
        });

        rest.execute(op).then(function(result) {
            expect(result).to.eql([]);
        }).finally(function() {
            JSONP.get.restore();
            done();
        });
    });

    it('should apply a normalize function if configured with JSONP', function(done) {
        var hermitCfg = new Hermit({id:'list_hermit_with_cfg', backend_cfg: {
            cors:{
                jsonp: true
            },
            normalize: function(response, op) {
                return {
                    response: response,
                    op:op,
                    normalized: true
                }
            }
        }});
        hermitCfg.getNetworkStateManager().setNetworkConnected(true);
        var f = new Hermit.OperationFactory(hermitCfg);
        var op = f.newOperation({ type: 'list', model: { key: 'test-model', backend_cfg: { key: 'test-backend-model' } } });
        var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

        sinon.stub(JSONP, "get", function(url, data, callback) {
            callback([]);
        });

        rest.execute(op).then(function(result) {
            console.dir(result);
            expect(result.response).to.eql([]);
            expect(result.op.id).to.eql(op.id);
            expect(result.normalized).to.be.true;
        }).finally(function() {
            JSONP.get.restore();
            done();
        });
    });

    describe('list', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'list', model: { key: 'test-model', group: 'my-group'} });
        });

        it('should perform a valid HTTP GET call to the backend', function(done) {
            hermit.getNetworkStateManager().setNetworkConnected(true);
            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
            backend.execute(op).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-model");
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.be.empty;
                done();
            }).catch(function(err) {
                done(err);
            }).finally(function() {
                if(window.ajax.restore)
                    window.ajax.restore();
            });
        });

        it('should support backend model key override', function(done){
            hermit.getNetworkStateManager().setNetworkConnected(true);
            var op2 = factory.newOperation({ type: 'list', model: { key: 'test-model', backend_cfg: { key: 'test-backend-model' } } });
            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
            backend.execute(op2).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-backend-model");
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.be.empty;
                done();
            }).catch(function(err) {
                done(err);
            }).finally(function() {
                if(window.ajax.restore)
                    window.ajax.restore();
            });
        });

        it('should append any query parameters provided in backend_cfg to the operation request', function(done) {
            var hermitCfg = new Hermit({id:'list_hermit_with_cfg', backend_cfg: {
                query:{
                    apikey: "test-api-key",
                    "token": 'default-injected-oauth2-token'
                }
            }});
            hermitCfg.getNetworkStateManager().setNetworkConnected(true);
            var f = new Hermit.OperationFactory(hermitCfg);
            var op = f.newOperation({ type: 'list', model: { key: 'test-model', backend_cfg: { key: 'test-backend-model' } } });
            var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
            rest.execute(op).then(function() {
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-backend-model?apikey=test-api-key&token=default-injected-oauth2-token");
            }).finally(function() {
                if(window.ajax.restore)
                    window.ajax.restore();
                done();
            });

        });
    });

    describe('show', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'show', model: { key: 'test-model', group: 'my-group'}, key: 'item1' });
            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
        });

        afterEach(function() {
            console.log("Looking to restore Ajax");
            if(window.ajax.restore) {
                window.ajax.restore();
                console.log("Ajax restored");
            }
        });

        it('should perform a valid HTTP GET call to the backend', function(done) {
            backend.execute(op).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-model/item1");
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.be.empty;
                done();
            }).catch(function(err) {
                done(err);
            });
        });

        it('should send a valid authorization header if provided', function(done){
            backend.setAuthorization("Token testappkey:0000000000000");

            backend.execute(op).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-model/item1");
                expect(window.ajax.args[0][1].headers.Authorization).to.equal("Token testappkey:0000000000000");
                expect(window.ajax.args[0][1].data).to.be.empty;
                done();
            }).catch(function(err) {
                done(err);
            });

        });

        it('should support backend model key override', function(done){
            var op2 = factory.newOperation({ type: 'show', model: { key: 'test-model', group: 'my-group',  backend_cfg: { key: 'test-backend-model' }  }, key: 'item1' });
            backend.execute(op2).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-backend-model/item1");
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.be.empty;
                done();
            }).catch(function(err) {
                done(err);
            });
        });

        it('should append any query parameters provided in backend_cfg to the operation request', function(done) {
            var hermitCfg = new Hermit({id:'show_hermit_with_cfg', backend_cfg: {
                query:{
                    apikey: "test-api-key",
                    "token": 'default-injected-oauth2-token'
                }
            }});
            hermitCfg.getNetworkStateManager().setNetworkConnected(true);
            var f = new Hermit.OperationFactory(hermitCfg);
            var op = f.newOperation({ type: 'show', model: { key: 'test-model', group: 'my-group',  backend_cfg: { key: 'test-backend-model' }  }, key: 'item1' });
            var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

            if(window.ajax.restore)
                window.ajax.restore();

            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
            rest.execute(op).then(function() {
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-backend-model/item1?apikey=test-api-key&token=default-injected-oauth2-token");
            }).finally(function() {
                if(window.ajax.restore)
                    window.ajax.restore();
                done();
            });

        });

    });

    describe('load', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'load', model: { key: 'test-model', group: 'my-group'}, filter: "age > 34" });
            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
        });

        afterEach(function() {
            if(window.ajax.restore)
                window.ajax.restore();
        });

        it('should perform a valid HTTP GET call to the backend with ', function(done) {
            backend.execute(op).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-model?filterExpr=&load=true&preflight=&");
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.be.empty;
                expect(result).to.eql([]);
                done();
            }).catch(function(err) {
                done(err);
            });
        });

        it('should support backend model key override', function(done){
            var op2 = factory.newOperation({ type: 'load', model: { key: 'test-model', group: 'my-group', backend_cfg: { key: 'test-backend-model' } }, filter: "age > 34" });
            backend.execute(op2).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-backend-model?filterExpr=&load=true&preflight=&");
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.be.empty;
                done();
            }).catch(function(err) {
                done(err);
            });
        });

        it('should be enqueued if no network is available', function(done) {
            var op2 = factory.newOperation({ type: 'load', model: { key: 'test-model', group: 'my-group', backend_cfg: { key: 'test-backend-model' } }, filter: "age > 34" });
            hermit.getNetworkStateManager().setNetworkConnected(false);
            sinon.spy(backend.queue, "enqueueOperation");
            try {
                var promise = backend.execute(op2);
                expect(backend.queue.enqueueOperation.callCount).to.equal(1);
                expect(promise).to.eql(backend.queue.enqueueOperation.args[0][0].deferredResult.promise);
                done();
            }
            catch(err) {
                console.log(err);
            }
            finally {
                console.log("Restoring backend");
                backend.queue.enqueueOperation.restore();
                hermit.getNetworkStateManager().setNetworkConnected(true);
            }
        });

        it('should append any query parameters provided in backend_cfg to the operation request', function(done) {
            var hermitCfg = new Hermit({id:'load_hermit_with_cfg', backend_cfg: {
                query:{
                    apikey: "test-api-key",
                    "token": 'default-injected-oauth2-token'
                }
            }});
            hermitCfg.getNetworkStateManager().setNetworkConnected(true);
            var f = new Hermit.OperationFactory(hermitCfg);
            var op = f.newOperation({ type: 'load', model: { key: 'test-model', group: 'my-group',  backend_cfg: { key: 'test-backend-model' }  }, key: 'item1' });
            var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

            if(window.ajax.restore)
                window.ajax.restore();

            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
            rest.execute(op).then(function() {
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-backend-model?apikey=test-api-key&token=default-injected-oauth2-token");
            }).finally(function() {
                if(window.ajax.restore)
                    window.ajax.restore();
                done();
            });

        });

    });

    describe('update', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'update', model: { key: 'test-model', group: 'my-group'}, key: 'test-instance-1', data: { change: false }});
            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
        });

        afterEach(function() {
            if(window.ajax.restore)
                window.ajax.restore();
        });

        it('should invoke commit on the operation after a successful update', function(done) {
            sinon.spy(op, "emit");
            backend.execute(op).then(function() {
                expect(op.emit.getCall(0).args[0]).to.equal('commit');
                done();
            }).catch(done).finally(function() {
                op.emit.restore();
            });

        });

        it('should support backend model key override', function(done){
            var op2 = factory.newOperation({ type: 'update', model: { key: 'test-model', group: 'my-group', backend_cfg: { key: 'test-backend-model' } }, key: 'test-instance-1', data: { change: false }});
            backend.execute(op2).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.equal('{"change":false}');
                done();
            }).catch(function(err) {
                done(err);
            });
        });

        it('should append any query parameters provided in backend_cfg to the operation request', function(done) {
            var hermitCfg = new Hermit({id:'update_hermit_with_cfg', backend_cfg: {
                query:{
                    apikey: "test-api-key",
                    "token": 'default-injected-oauth2-token'
                }
            }});
            hermitCfg.getNetworkStateManager().setNetworkConnected(true);
            var f = new Hermit.OperationFactory(hermitCfg);
            var op = f.newOperation({ type: 'update', model: { key: 'test-model', group: 'my-group',  backend_cfg: { key: 'test-backend-model' }  }, key: 'item1' });
            var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

            if(window.ajax.restore)
                window.ajax.restore();

            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
            rest.execute(op).then(function() {
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-backend-model/item1?apikey=test-api-key&token=default-injected-oauth2-token");
            }).finally(function() {
                if(window.ajax.restore)
                    window.ajax.restore();
                done();
            });

        });

    });

    describe('insert', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'insert', model: { key: 'test-model', group: 'my-group'}, key: 'test-instance-1', data: { change: false }});
            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
        });

        afterEach(function() {
            if(window.ajax.restore)
                window.ajax.restore();
        });

        it('should invoke commit on the operation after a successful insert', function(done) {
            sinon.spy(op, "emit");

            backend.execute(op).then(function() {
                expect(op.emit.getCall(0).args[0]).to.equal('commit');
                done();
            }).catch(done).finally(function() {
                op.emit.restore();
            });

        });

        it('should support backend model key override', function(done){
            var op2 = factory.newOperation({ type: 'insert', model: { key: 'test-model', group: 'my-group', backend_cfg: { key: 'test-backend-model' } }, key: 'test-instance-1', data: { change: false }});
            backend.execute(op2).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.equal('{"change":false}');
                done();
            }).catch(function(err) {
                done(err);
            });
        });

        it('should append any query parameters provided in backend_cfg to the operation request', function(done) {
            var hermitCfg = new Hermit({id:'insert_hermit_with_cfg', backend_cfg: {
                query:{
                    apikey: "test-api-key",
                    "token": 'default-injected-oauth2-token'
                }
            }});
            hermitCfg.getNetworkStateManager().setNetworkConnected(true);
            var f = new Hermit.OperationFactory(hermitCfg);
            var op = f.newOperation({ type: 'insert', data: {id:1}, model: { key: 'test-model', group: 'my-group',  backend_cfg: { key: 'test-backend-model' }  }, key: 'item1' });
            var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

            if(window.ajax.restore)
                window.ajax.restore();

            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
            rest.execute(op).then(function() {
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-backend-model?apikey=test-api-key&token=default-injected-oauth2-token");
            }).finally(function() {
                if(window.ajax.restore)
                    window.ajax.restore();
                done();
            });

        });

    });

    describe('upsert', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'upsert', model: { key: 'test-model', group: 'my-group'}, key: 'test-instance-1', data: { change: false }});
            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
        });

        afterEach(function() {
            if(window.ajax.restore)
                window.ajax.restore();
        });

        it('should invoke commit on the operation after a successful upsert', function(done) {
            sinon.spy(op, "emit");

            backend.execute(op).then(function() {
                expect(op.emit.getCall(0).args[0]).to.equal('commit');
                done();
            }).catch(done).finally(function() {
                op.emit.restore();
            });

        });

        it('should support backend model key override', function(done){
            var op2 = factory.newOperation({ type: 'upsert', model: { key: 'test-model', group: 'my-group', backend_cfg: { key: 'test-backend-model' } }, key: 'test-instance-1', data: { change: false }});
            backend.execute(op2).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.equal('{"change":false}');
                done();
            }).catch(function(err) {
                done(err);
            });
        });

        it('should append any query parameters provided in backend_cfg to the operation request', function(done) {
            var hermitCfg = new Hermit({id:'upsert_hermit_with_cfg', backend_cfg: {
                query:{
                    apikey: "test-api-key",
                    "token": 'default-injected-oauth2-token'
                }
            }});
            hermitCfg.getNetworkStateManager().setNetworkConnected(true);
            var f = new Hermit.OperationFactory(hermitCfg);
            var op = f.newOperation({ type: 'upsert', model: { key: 'test-model', group: 'my-group',  backend_cfg: { key: 'test-backend-model' }  }, key: 'item1' });
            var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

            if(window.ajax.restore)
                window.ajax.restore();

            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
            rest.execute(op).then(function() {
                expect(window.ajax.args[0][0]).to.contain("http://localhost:8000/test-backend-model/item1?apikey=test-api-key&token=default-injected-oauth2-token");
            }).finally(function() {
                if(window.ajax.restore)
                    window.ajax.restore();
                done();
            });

        });

    });

    describe('remove', function() {
        var op;

        beforeEach(function() {
            op = factory.newOperation({ type: 'remove', model: { key: 'test-model', group: 'my-group'}, key: 'test-instance-1'});
            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
        });

        afterEach(function() {
            if(window.ajax.restore)
                window.ajax.restore();
        });

        it('should invoke commit on the operation after a successful remove', function(done) {
            sinon.spy(op, "emit");

            backend.execute(op).then(function() {
                expect(op.emit.getCall(0).args[0]).to.equal('commit');
                done();
            }).catch(done).finally(function() {
                op.emit.restore();
            });

        });

        it('should support backend model key override', function(done){
            var op2 = factory.newOperation({ type: 'remove', model: { key: 'test-model', group: 'my-group', backend_cfg: { key: 'test-backend-model' } }, key: 'test-instance-1'});
            backend.execute(op2).then(function(result) {
                expect(window.ajax.callCount).to.equal(1);
                expect(window.ajax.args[0][1].headers).to.eql({'Content-Type': 'application/json'});
                expect(window.ajax.args[0][1].data).to.be.empty;
                done();
            }).catch(function(err) {
                done(err);
            });
        });

        it('should append any query parameters provided in backend_cfg to the operation request', function(done) {
            var hermitCfg = new Hermit({id:'remove_hermit_with_cfg', backend_cfg: {
                query:{
                    apikey: "test-api-key",
                    "token": 'default-injected-oauth2-token'
                }
            }});
            hermitCfg.getNetworkStateManager().setNetworkConnected(true);
            var f = new Hermit.OperationFactory(hermitCfg);
            var op = f.newOperation({ type: 'remove', model: { key: 'test-model', group: 'my-group',  backend_cfg: { key: 'test-backend-model' }  }, key: 'item1' });
            var rest = new Hermit.RestBackend(hermitCfg, {baseUrl : "http://localhost:8000"});

            if(window.ajax.restore)
                window.ajax.restore();

            sinon.stub(window, "ajax", function(url, opts) {
                return opts.success("[]", {});
            });
            rest.execute(op).then(function() {
                expect(window.ajax.args[0][0]).to.include("http://localhost:8000/test-backend-model/item1?apikey=test-api-key&token=default-injected-oauth2-token");
            }).finally(function() {
                if(window.ajax.restore)
                    window.ajax.restore();
                done();
            });

        });

    });

});
