
describe('Hermit', function() {

    before(Hermit.reset);

    describe('Hermit:constructor', function() {

        it('should NOT register hermit instance without an id', function() {
            var hermit = new Hermit();
            var hermit2 = Hermit.getInstance();
            expect(hermit2).to.be.undefined;
        });

        it('should be declared globally in the browser', function() {
            expect(Hermit).not.to.be.null;
        });

        it('should create hermit instance with default options', function() {
            var hermit = new Hermit();
            expect(hermit).not.to.be.null;
            expect(hermit.opts).not.to.be.null;
        });
        it('should register hermit instance with an id', function() {
            var hermit = new Hermit({id: 'hermit1'});
            var hermit2 = Hermit.getInstance('hermit1');
            expect(hermit2).to.equal(hermit);
        });

        it('should create a DefaultModelProvider if none is provided', function() {
            var hermit = new Hermit();
            expect(hermit.getModelProvider()).to.be.an.instanceof(Hermit.DefaultModelProvider);
        });

        it('should create a DefaultNetworkStateManager if none is provided', function() {
            var hermit = new Hermit();
            expect(hermit.getNetworkStateManager()).to.be.an.instanceof(Hermit.DefaultNetworkStateManager);
        });

        it('should create a default operation factory if none is provided', function() {
            var hermit = new Hermit();
            expect(hermit.getOperationFactory()).to.be.an.instanceof(Hermit.OperationFactory);
        });
    });

    describe('Hermit:getInstance', function(){
        it('should return the specified instance', function() {
            var hermit = new Hermit({id: 'hermit1'});
            var hermit2 = Hermit.getInstance('hermit1');
            expect(hermit2).to.equal(hermit);
        });

        it('should return the first instance when no id is specified', function() {
            var hermit = new Hermit({id: 'hermit1'});
            var hermit2 = Hermit.getInstance();
            expect(hermit2).to.equal(hermit);
        });
        it('should return undefined when an invalid id is specified', function() {
            var hermit = new Hermit({id: 'hermit1'});
            var hermit2 = Hermit.getInstance('hermit4');
            expect(hermit2).to.be.undefined;
        });

    });

    describe('Hermit:getBackend', function() {


        it('should provide a valid backend', function(){
            var backend = Hermit.getInstance().getBackend();
            expect(backend).to.be.an.instanceof(Hermit.Backend);
        });
        it('backend should have a valid baseUrl', function(){
            var backend = Hermit.getInstance().getBackend();
            expect(backend.baseUrl).not.to.be.undefined;
        });

        describe('InvalidBackend', function(){
            var backend;

            before(function() {
                console.log("IN BEFORE!!!");
                backend = Hermit.Backend;
                delete Hermit.Backend;
            });

            after(function() {
                Hermit.Backend = backend;
            });

            it('should throw an InvalidConfiguration exception if Hermit.Backend is undefined', function() {
                var backend;

                try {
                    var hermit = new Hermit();
                    hermit.getBackend();
                    expect(true).to.be.false;
                }
                catch(err) {
                    expect(err).to.be.an.instanceof(Hermit.Errors.InvalidConfiguration);
                }

            });
        });
    });

    describe('Hermit:model', function() {
        var hermit;

        before(function() {
            hermit = new Hermit();
            hermit.getModelProvider().registerModel({
                key: 'products'
            });
        });

        it('should return an undefined model if an invalid key is provided', function() {
            var model = hermit.model('invalid-test-model');
            expect(model).to.be.undefined;
        });

        it('should return a valid model instance if a valid model key is provided', function() {
            var model = hermit.model('products');
            expect(model).not.to.be.undefined;
            expect(model).to.be.an.instanceof(Hermit.Model);
            expect(model.key).to.eql('products');
        });
    });

});
