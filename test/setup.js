// Export modules to global scope as necessary (only for testing)
if (typeof process !== 'undefined' && process.title === 'node') {
    // We are in node. Require modules.
    expect = require('chai').expect;
    sinon = require('sinon');
    Hermit = require('../src/hermit');
    isBrowser = false;
} else {
    // We are in the browser. Set up variables like above using served js files.
    expect = chai.expect;
    assert = chai.assert;
    should = chai.should;

    credentials = {
        "url": "http://localhost:8000",
        "apikey": "c847ffc1c8962b1f5097251b7630e08141a12d79",
        "secret": "c93236086b8fc4312c9b3890384f01c9694ea41f",
        "token": "8325ccdd2542f533db283d2c8c3fe6ee3987b006"
    };

    config = {
        id: 'initial-load-test-device',
        group: 'initial-load-test-device',
        baseUrl: credentials.url,
        backend_cfg: {
            headers: {
                Authorization: "Token " + credentials.apikey + ":" + credentials.token
            }
        },
        sync_agent_cfg:{
            subscribe_key: "sub-c-7c3ae000-1667-11e4-92d2-02ee2ddab7fe",
            publish_key: "pub-c-e87227e1-d2a8-427f-be8d-3c5ce3c8f0ad"
        }
    };

    TestSetup = {
        initializeModels: function(hermit) {

            // Register all models
            hermit.getModelProvider().registerModel({
                key: 'colors',
                cache: true,
                sync: true,
                ds: hermit.DS('e2e-ds', { max: '10MB' })
            });

        }
    };

    // num and sinon already exported globally in the browser.
    isBrowser = true;
}
