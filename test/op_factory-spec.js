describe('OperationFactory', function() {
    var factory, hermit;

    before(function() {
        hermit = new Hermit({id: 'op-factory-test'});
        factory = new Hermit.OperationFactory(hermit);
        hermit.getNetworkStateManager().setNetworkConnected(true);
    });

    describe('newOperation', function() {

        it('should generate a valid SHA-1 hash from the operation spec', function() {
            var op = factory.newOperation({type: 'upsert', key: 'test-key', data: {field1: 'value', field2: 1, field3: true }});
            expect(op).not.to.be.undefined;
            expect(op.hash).not.to.be.undefined;
        });

        it('should throw an exception is no operation type is provided', function() {
            try {
                factory.newOperation({key: 'test-key', data: {field1: 'value', field2: 1, field3: true }});
            }
            catch(err) {
                expect(err).to.be.an.instanceof(Hermit.Errors.InvalidParameter);
            }
        });

        it('should through an exception if we provide an unsupported operation type', function() {
            try {
                factory.newOperation({type: 'unsupported', key: 'test-key', data: {field1: 'value', field2: 1, field3: true }});
            }
            catch(err) {
                expect(err).to.be.an.instanceof(Hermit.Errors.InvalidOperation);
            }
        });

        it('should generate unique key to use when caching similar operations', function() {
            var op1 = factory.newOperation({type: 'upsert', key: 'test-key', model: { key: 'my-model-key'}, data: {field1: 'value', field2: 1, field3: true }});
            var op2 = factory.newOperation({type: 'upsert', key: 'test-key', model: { key: 'my-model-key'}, data: {field1: 'value', field2: 1, field3: true }});
            expect(op1.hash).to.equal(op2.hash);
            expect(op1.id).not.to.equal(op2.id);
            expect(op1.uniqueKey()).to.equal("82ac5b0b61945aaabcb61fef9d09c2e71db105b5");
            expect(op1.uniqueKey()).to.equal(op2.uniqueKey());
        });

    });


});