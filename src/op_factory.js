(function(Hermit) {

    Hermit.Operations = {};
    Hermit.WaitingOperations = {};

    Hermit.Operation = function(spec, _hermit) {
        console.log("Operation:__cstor");
        this._hermit = _hermit;

        EventEmitter2.call(this);

        // Setup model options (with valid defaults)
        _.merge(this, spec);

        this.options = spec.options || {};
        this.stereotype = spec.stereotype;
        this.subType = spec.subType;
        this.subItem = spec.subItem;
        this.ts = Date.now();

        // Compute the operation unique hash key
        var payload = _.omit(spec, "model");
        if(spec.model)
            payload.model = spec.model.key;
        payload = JSON.stringify(payload, null, '');
        var shaObj = new jsSHA(payload, "TEXT");
        this.hash = shaObj.getHash("SHA-1", "HEX");
        this.id = this.hash +":"+ this.ts + ":" + Math.random().toString();
        if(spec.model)
            this.id = this.id + ":" + spec.model.key;
    };

    Hermit.Operation.prototype = Object.create(EventEmitter2.prototype);
    Hermit.Operation.prototype.constructor = Hermit.Operation;

    Hermit.Operation.makeUniqueKey = function(model, op, id, options) {
        console.log("Hermit.Operation:makeUniqueKey", arguments);
        options = options || {};
        var payload = model;
        payload += ":" + op;
        if(id)
            payload += ":" + id;

        if(options.clear) {
            return payload;
        }
        else {
            var shaObj = new jsSHA(payload, "TEXT");
            return shaObj.getHash("SHA-1", "HEX");
        }
    };

    /**
     * This is the unique key used when caching this operation result. It can be reconstructed
     * easily by other operation to perform cache management (invalidate another operation cache when and update or insert result happens)
     * @returns {*}
     */
    Hermit.Operation.prototype.uniqueKey = function(clear) {
        return Hermit.Operation.makeUniqueKey(this.model.key, this.type, this.key, { clear: clear});
    };

    Hermit.Operation.prototype.toJSON = function() {
        var json = _.pick(this, "id", "hash", "data", "result", "type", "stereotype", "key", "model", "options", "cache", "sync", "ds");
        json.model = this.model.key;
        return json;
    };

    Hermit.Operation.plain = function(ops, results) {
        var result = [];

        /* istanbul ignore else */
        if(!_.isArray(ops))
            ops = [ops];

        // Handle any results we may have
        if(results) {
            if(!_.isArray(results)) {
                results = [results];
            }
        }

        _.each(ops, function(op, idx) {
            var json = op.toJSON();
            json.data = _.omit(json.data, "__temp", "__op", "__removed", "$temp", "$op", "$removed");

            // Apply any results
            if(results && results.length > idx)
                json.result = results[idx];

            result.push(json);
        });

        return result;
    };

    function createOperation(ops, name) {
        ops[name] = function() {
            Hermit.Operation.apply(this, arguments);
            this.options = this.options || {};
        };
        ops[name].prototype = Object.create(Hermit.Operation.prototype);
        ops[name].prototype.constructor = ops[name];
        ops[name].prototype.execute = function() {
            var _this = this;

            var exec = Hermit.Operations[this.type];
            this._hermit.emit('start-operation', this);

            var promise = exec.call(this).then(function(result) {
                console.log("Operation %s was successfully completed", _this.id);
                _this._hermit.emit('complete-operation', _this, result);
                return result;
            }).catch(function(err) {
                console.log("Operation %s failed", _this.id);
                _this._hermit.emit('failed-operation', _this, err);
                throw err;
            });

            if(this.options.waitForConfirmation || this.options.waitForCommit ) {
                console.log("Registering operation %s for waiting", this.hash);
                Hermit.WaitingOperations[this.hash] = {
                    op: this,
                    promise: new Promise(function(resolve, reject) {

                        if(this.options.waitForConfirmation) {

                            // Register for confirmation even on this operation
                            this.once('confirmed', function(result) {
                                console.log("Operation %s is confirmed", this.hash);

                                if(!_.isArray(result))
                                    result = [result];

                                this._hermit.emit('confirmed-operation', this);

                                resolve(result);
                            });

                        }
                        else if(this.options.waitForCommit) {

                            // Register for confirmation even on this operation
                            this.once('committed', function(result) {
                                console.log("Operation %s is committed", this.hash);

                                if(!_.isArray(result))
                                    result = [result];

                                resolve(result);
                            });

                        }

                        // If an error occurs, we fail fast.
                        promise.then(function(result) {
                            _this.emit('local-apply', result);
                            return result;
                        }).catch(function(err) {
                            _this.emit('operation-error', err);
                            delete Hermit.WaitingOperations[_this.hash];
                            reject(err);
                        });

                    }.bind(this)).timeout(this.options.timeout || 30000)
                };

                return Hermit.WaitingOperations[this.hash].promise;
            }
            else {
                return promise;
            }
        };
    }

    var Operations = {};

    createOperation(Operations, 'list');
    createOperation(Operations, 'insert');
    createOperation(Operations, 'update');
    createOperation(Operations, 'remove');
    createOperation(Operations, 'show');
    createOperation(Operations, 'upsert');
    createOperation(Operations, "load");

    var OpFactory = function(_hermit) {
        this._hermit = _hermit;
    };

    OpFactory.prototype.newOperation = function(spec) {
        if(!spec.type) throw new Hermit.Errors.InvalidParameter("spec.type");

        var OpCls = Operations[spec.type];
        if(OpCls) {
            return new OpCls(spec, this._hermit);
        }
        else {
            throw new Hermit.Errors.InvalidOperation(spec.type);
        }
    };

    /**
     * Used when we receive an operation spec from the sync server
     *
     * @param spec
     */
    OpFactory.prototype.wrap = function(spec) {
        if(!spec.type) throw new Hermit.Errors.InvalidParameter("spec.type");

        var OpCls = Operations[spec.type];
        if(OpCls) {
            var op = new OpCls(spec, this._hermit);

            // Make sure to reuse the hash and id
            op.hash = spec.hash;
            op.id = spec.id;

            return op;
        }
        else {
            throw new Hermit.Errors.InvalidOperation(spec.type);
        }
    };

    Hermit.OperationFactory = OpFactory;

})(Hermit);
