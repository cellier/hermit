(function(Hermit) {

    var DMP = function(_hermit) {
        this._hermit = _hermit;
        this.models = {};
    };

    DMP.prototype.getModel = function(modelKey) {
        return this.models[modelKey];
    };

    DMP.prototype.registerModel = function(modelSpec) {
        var model = new Hermit.Model(_.defaults(modelSpec, {
            hermitId: this._hermit.id,
            max_sync_delay: 0
        }), this._hermit.getBackend());
        this.models[modelSpec.key] = model;
        return model;
    };

    DMP.prototype.visitModels = function(visitor) {
        console.log("DMP:visitModels");
        var _this = this;
        return Promise.each(_.keys(this.models), function(modelKey) {
            return visitor.call(_this._hermit, _this.models[modelKey]);
        });
    };

    Hermit.DefaultModelProvider = DMP;

})(Hermit);
