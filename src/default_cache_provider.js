(function(Hermit) {

    function MemoryCache() {
        this.entries = {};
    }

    MemoryCache.prototype.getEntry = function(key) {
        console.log("MemoryCache:getEntry", key);
        if(_.isString(key))
            return Promise.resolve(this.entries[key]);
        else {
            var op = key;
            return Promise.resolve(this.entries[op.hash]);
        }
    };

    MemoryCache.prototype.setEntry = function(key, data, options) {
        var _self = this;
        options = options || {};

        this.entries[key] = {
            key: key,
            data: data,
            ikey: options.ikey || key, // invalidation key
            ts: Date.now()
        };

        if(options.expire) {

            if(_.isString(options.expire)) {
                var expireExpr = later.parse.text(options.expire);
                console.log(options.expire, expireExpr);
                this.entries[key].watchdog = later.setTimeout(function() {
                    // Remove this cache entry, forcing a reload for any future call
                    _self.invalidate(key);
                }, expireExpr);
            }
            else {
                this.entries[key].watchdog = setTimeout(function() {
                    _self.invalidate(key);
                }, options.expire);
            }
        }

    };

    MemoryCache.prototype.invalidate = function(key) {
        console.log("MemoryCache:invalidate", key);
        var entries = _.values(this.entries), _self = this;
        _.each(entries, function(entry) {
            if(entry.ikey === key) {
                console.log("Invalidating entry %s(s)", entry.key, entry.ikey);
                delete _self.entries[key];
            }
        });
    };

    var DCP = function(_hermit) {
        this._hermit = _hermit;
        this.caches = {};
    };

    // The default cache is MemoryCache. May be overriden through plugins
    if(!Hermit.CacheClass) {
        console.log("WARN: No configured cache class. Using a MemoryCache");
        Hermit.CacheClass = MemoryCache;
    }
    else
        console.log("Using cache class", Hermit.CacheClass);

    DCP.prototype.getCache = function(cacheKey) {
        console.log("DefaultCacheManager:getCache", cacheKey);
        if(!cacheKey) return;

        var cache = this.caches[cacheKey];
        if(!cache) {
            cache = new Hermit.CacheClass(this, cacheKey);
            this.caches[cacheKey] = cache;
        }
        return cache;
    };

    Hermit.DefaultCacheProvider = DCP;

})(Hermit);
