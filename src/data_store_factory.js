(function(Hermit) {

    function DSF(hermit, options) {
        this.stores = {};
    }

    DSF.prototype.openDS = function(hermitId, modelKey, options) {
        var defer = Promise.defer(), _self = this;

        setTimeout(function() {
            console.log("Opening datastore %s", modelKey);

            /* istanbul ignore if */
            if(!Hermit.DataStoreClass) {
                console.log("Unable to locate datastore class");
                defer.reject(new Hermit.Errors.InvalidConfiguration("missing.datastore.plugin"));
            }

            var ds = _self.stores[hermitId+modelKey];
            if(!ds) {
                console.log("Initializing datastore %s for first time", hermitId+":"+modelKey);
                /* istanbul ignore if */
                if(!Hermit.DataStoreClass) {
                    defer.reject(new Hermit.Errors.InvalidConfiguration("missing-datastore-plugin"));
                }
                else {
                    ds = new Hermit.DataStoreClass(hermitId, modelKey, options);
                    _self.stores[hermitId+modelKey] = ds;

                    if(_.isFunction(ds.initialize)) {
                        defer.resolve(ds.initialize());
                    }
                    else
                        defer.resolve(ds);
                }

            }
            else {
                console.log("Reusing existing datastore", ds);
                defer.resolve(ds);
            }
        }, 0);

        return defer.promise;
    };

    Hermit.DataStoreFactory = DSF;

})(Hermit);
