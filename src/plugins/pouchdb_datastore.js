(function(Hermit) {

    function PouchDS(hermitId, modelKey, options) {
        console.log("PouchDS:__constructor");
        options = options || {};
        this.db_name =  hermitId + "_" + (options.namespace||"") + "_" + modelKey;
        this.idField = options.idField;
        this.auto_id = options.auto_id;

        if(!window.Hermit) {
            throw new Error("hermit.js must be loaded before this add-on");
        }

        if(!window.PouchDB) {
            throw new Error("Please add pouchdb before loading this add-on");
        }

        console.log("Opening PouchDB %s", this.db_name);

        try {
            this.db = new PouchDB(this.db_name, { auto_compaction: true });
        }
        catch(err) {
            console.log("UNABLE TO CREATRE POUCHDB INSTANCE %s", this.db_name, err);
            throw err;
        }
    }

    PouchDS.prototype.list = function(options) {
        options = options || {};
        return new Promise(function(resolve, reject) {
            var _this = this;

            _this.db.allDocs({include_docs: true}, function(err, response) {
                if(err) {
                    reject(err);
                }
                else if(response) {

                    var docs = _.map(response.rows, function(row) { return row.doc });

                    // Apply any provided query first
                    if(options.query) {
                        docs = _.query(docs, options.query);
                    }

                    // Post-process result according to any provided query and temp data flags
                    docs = _.filter(docs, function(row) {

                        if(!row.$temp || options.includeTemp ) {

                            if(!row.$removed || options.includeRemoved) {
                                return row;
                            }
                            else {
                                console.log("item %s was skipped as being removed", row[_this.idField || 'id']);
                            }

                        }
                        else {
                            console.log("item %s was skipped as being temp", row[_this.idField || 'id']);
                        }

                    });

                    if(options.sortBy) {
                        docs = _.sortBy(docs, options.sortBy);
                        if(options.descending) {
                            docs.reverse();
                        }
                    }

                    if(options.skip && docs.length > options.skip) {
                        docs.splice(0, options.skip);
                    }

                    if(options.limit) {
                        docs.splice(options.limit);
                    }

                    if(_.isFunction(options.normalize)) {
                        resolve(options.normalize.call(_this,docs, options));
                    }
                    else {
                        resolve({data: docs, _pagination: { total: docs.length} });
                    }
                }
                else
                    resolve({data: []});
            });

        }.bind(this));
    };

    PouchDS.prototype.show = function(id, options) {
        options = options || {};
        return new Promise(function(resolve, reject) {
            this.db.get(id.toString(), function(err, doc) {
                if(err)
                    reject(err);
                else {
                    if(!doc.$removed || options.includeRemoved) {
                        if (!doc.$temp || options.includeTemp) {
                            return resolve(doc)
                        }
                    }

                    // Resolve to undefined as we didn't find the specified object
                    return resolve();
                }
            });
        }.bind(this));
    };

    PouchDS.prototype.insert = function(data, options) {
        options = options || {};

        if(!_.isArray(data)) {
            data = [data];
        }

        return new Promise(function(resolve, reject) {
            var _this = this;

            console.log("Looping through %d data element(s) to insert", data.length);

            return Promise.each(data, function(obj) {
                console.log("Inserting element", obj);
                var id = obj.id;
                if(_this.idField) {
                    id = obj[_this.idField];
                }
                if(id || _this.auto_id) {
                    console.log("Inserting document", id);

                    // Keep revision to avoid conflicts
                    obj.$revision = obj.__v;
                    delete obj.__v;

                    if(id)
                        obj._id = id.toString();

                    if(!options.commit) {
                        obj.$temp = true;
                        obj.$op = options.op;
                    }
                    else
                        console.log("Commit option has been selected. Inserting permanent data");

                    if(obj._id)
                        return _this.db.put(obj).then(function(result) {
                            obj.$revision = result.rev;
                            return result;
                        });
                    else
                        return _this.db.post(obj).then(function(result) {
                            console.dir(result);
                            obj.$revision = result.rev;
                            return result;
                        });
                }
                else {
                    throw new Error("no id found");
                }
            }).then(function() {
                resolve(data);
            }).catch(function(err) {
                reject(err);
            });

        }.bind(this));
    };

    PouchDS.prototype.update = function(id, data, options) {
        options = options || {};
        return new Promise(function(resolve, reject) {
            var _this = this;

            _this.db.get(id.toString(), function(err, doc) {
                if(err)
                    reject(err);
                else {

                    if(doc) {
                        data[_this.idField || 'id'] = id.toString();

                        if(!options.commit) {
                            data.$temp = true;
                            data.$op = options.op;
                        }

                        _.merge(doc, data);

                        doc._id = id.toString();

                        _this.db.put(doc, function(err, result) {
                            if(err) {
                                console.log("Unable to update data", id, data, err);
                                reject(err);
                            }
                            else if(result.ok) {
                                // Record our current revision to support operation reverting
                                doc.$revision = result.rev;
                                resolve(doc);
                            }
                            else
                                reject({success: false});
                        });
                    }
                    else {
                        reject(new Error("unknown-key:"+id.toString()));
                    }
                }
            });
        }.bind(this));
    };

    PouchDS.prototype.remove = function(id, options) {
        options = options || {};
        return new Promise(function(resolve, reject) {
            var _this = this;

            if(options.commit) {
                this.db.remove(id.toString(), function(err) {
                    if(err) {
                        reject(err);
                    }
                    else {
                        resolve(true);
                    }
                });
            }
            else {
                console.log("Looking for elm ", id);
                this.show(id).then(function(elm) {
                    elm.$removed = true;
                    elm.$temp = true;
                    elm.$op = options.op;

                    _this.db.put(elm, function(err, result) {
                        if(err) {
                            reject(err);
                        }
                        else {
                            elm.$revision = result.rev;
                            resolve(elm);
                        }
                    });
                });
            }
        }.bind(this));
    };

    PouchDS.prototype.upsert = function(id, data, options) {
        options = options || {};
        return new Promise(function(resolve, reject) {
            var _this = this;

            this.show(id.toString()).then(function(doc){
                data[_this.idField||'id'] = id.toString();

                if(!options.commit) {
                    data.$temp = true;
                    data.$op = options.op;
                }

                _.merge(doc, data);
                doc._id =  id.toString();

                _this.db.put(doc, function(err, result) {
                    if(err) {
                        reject(err);
                    }
                    else {
                        doc.$revision = result.rev;
                        resolve(doc);
                    }
                });
            }).catch(function() {
                data._id = id.toString();

                if(!options.commit) {
                    data.$temp = true;
                    data.$op = options.op;
                }

                _this.db.put(data, function(err, result) {
                    if(err) {
                        reject(err);
                    }
                    else {
                        data.$revision = result.rev;
                        resolve(data);
                    }
                });
            });

        }.bind(this));
    };

    PouchDS.prototype.reset = function() {
        return new Promise(function(resolve, reject) {
            var _this = this;
            _this.db.destroy(function(err) {
                if(err) {
                    reject(err);
                }
                else {
                    // Recreate a new empty database
                    _this.db = new PouchDB(_this.db_name, { auto_compaction: true });
                    resolve();
                }
            });
        }.bind(this));
    };

    PouchDS.prototype.synced = function(delta) {
        console.log("PouchDS:Synced", delta);
        // TODO: If this datastore has been sync'ed recently enough
        return true;
    };

    PouchDS.prototype.revert = function(id, $revision) {
        console.log("PouchDS:revert", id, $revision);
        return new Promise(function(resolve, reject) {
            this.db.remove(id, $revision, function(err) {
                if(err) {
                    reject(err);
                }
                else {
                    resolve(true);
                }
            });
        }.bind(this));
    };

    PouchDS.prototype.confirm = function(hash) {
        console.log("PouchDS:confirm", hash);
        return new Promise(function(resolve, reject) {
            var toRemove = [], _this = this;

            this.db.allDocs({include_docs: true}, function(err, records) {
                var affectedRecords = _.map(_.filter(records.rows, function(r) {
                    return r.doc.$temp && r.doc.$op === hash
                }), function(r){ return r.doc; });

                console.log("Found %d affected record(s) to confirm", affectedRecords.length);

                _.each(affectedRecords, function(record) {

                    if(record.$removed) {
                        toRemove.push(record);
                    }
                    else {
                        delete record.$temp;
                        delete record.$op;
                        _this.db.put(record);
                    }
                });

                console.log("Found %d record to remove", toRemove.length);
                Promise.each(toRemove, function(elm) {
                    console.log("Removing record %s", elm._id);
                    return new Promise(function(res, rej) {
                        _this.db.remove(elm._id, elm._rev, function(err) {
                            if(err) {
                                console.log("Remove Error:", err);
                                rej(err);
                            }
                            else {
                                console.log("Record %s was successfully removed", elm._id);
                                res(elm);
                            }
                        });
                    });
                }).then(function() {
                    resolve({confirmed: affectedRecords.length, removed: toRemove.length});
                }).catch(function(err) {
                    reject(err);
                });

            });

        }.bind(this));
    };

    Hermit.DataStoreClass = Hermit.PouchDS = PouchDS;

})(Hermit);
