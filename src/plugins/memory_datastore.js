(function(Hermit) {

    function MemoryDS(hermitId, modelKey, options) {
        this._hermit = Hermit.getInstance(hermitId);
        this.modelKey = modelKey;
        this.options = options || {};
        this.data = [];
    }

    MemoryDS.prototype.list = function(options) {
        var _this = this;
        console.log("MemoryDS:list");
        options = options || {};

        return new Promise(function(resolve) {
            var data = _.filter(_this.data, function(d) {
                console.log("Processing item %s", d.id, d.__removed);

                if(!d.__removed || options.includeRemoved) {

                    if(!d.__temp || options.includeTemp ) {
                        return d;
                    }
                    else {
                        console.log("item %s was skipped as being temp", d.id);
                    }
                }
                else {
                    console.log("item %s was skipped as being removed", d.id);
                }
            });

            console.log("Returned %d elements", data.length);
            resolve(data);
        });
    };

    MemoryDS.prototype.show = function(id, keyField, options) {
        var _this = this;
        options = options || {};
        console.log("MemoryDS:show ", id, keyField);
        return new Promise(function(resolve) {
            keyField = keyField || 'id';
            var matching = _.filter(_.map(_this.data, function(d) {
                if(d[keyField] === id)
                    return d;
            }), function(i) { return i});

            if(matching.length > 0) {
                console.log("Found item %s,", id, matching[0]);

                if(!matching[0].__removed || options.includeRemoved) {
                    if (!matching[0].__temp || options.includeTemp) {
                        return resolve(matching[0])
                    }
                }

                // Resolve to undefined as we didn't find the specified object
                return resolve();
            }
            else
                return resolve();
        });
    };

    MemoryDS.prototype.insert = function(elm, options) {
        console.log("MemoryDS:insert");
        options = options || {};
        var _this = this;

        if(!_.isArray(elm)) {
            elm = [elm];
        }

        return Promise.all(Promise.each(elm, function(e) {
            return new Promise(function(resolve) {
                if(!e.id)
                    e.id = e.key;

                /* istanbul ignore else */
                if(!options.commit) {
                    e.__temp = true;
                    e.__op = options.op;
                }

                console.log("Inserting element:",e);
                return resolve(_this.data.push(e));
            });
        }));

    };

    MemoryDS.prototype.update = function(id, data, options) {
        var _this = this;
        console.log("MemoryDS:update", arguments);
        options = options || {};
        return new Promise(function(resolve, reject) {
            var elm = _.find(_this.data, function(d) { return d.id === id});
            if(elm) {
                _.merge(elm, data);

                /* istanbul ignore else */
                if(!options.commit) {
                    elm.__temp = true;
                    elm.__op = options.op;
                }

                resolve(elm);
            }
            else {
                reject();
            }
        });
    };

    MemoryDS.prototype.remove = function(id, options) {
        var _this = this;
        console.log("MemoryDS:remove", id);
        options = options || {};
        return new Promise(function(resolve, reject){
            var elm = _.find(_this.data, function(d) { return d.id === id});
            if(elm) {

                /* istanbul ignore else */
                if(!options.commit) {
                    elm.__removed = true;
                    elm.__temp = true;
                    elm.__op = options.op;
                }

                resolve(elm);
            }
            else
                reject();
        });
    };

    MemoryDS.prototype.upsert = function(id, data, options) {
        var _this = this;
        console.log("MemoryDS:upsert", id);
        options = options || {};
        return new Promise(function(resolve) {
            var elm = _.find(_this.data, function(d) { return d.id === id});
            if(elm) {

                /* istanbul ignore else */
                if(!options.commit) {
                    elm.__temp = true;
                    elm.__op = options.op;
                }
                _.merge(elm, data);
            }
            else {
                elm = data;

                /* istanbul ignore else */
                if(!data.id)
                    elm.id = data.key;

                /* istanbul ignore else */
                if(!options.commit) {
                    elm.__temp = true;
                    elm.__op = options.op;
                }

                _this.data.push(elm);
            }
            resolve(elm);
        });
    };

    MemoryDS.prototype.reset = function() {
        console.log("MemoryDS:reset");
        return new Promise(function(resolve){
            this.data = [];
            resolve(true);
        }.bind(this));
    };

    MemoryDS.prototype.synced = function(delta) {
        console.log("MemoryDS:Synced", delta);
        // TODO: If this datastore has been sync'ed recently enough
        return true;
    };

    MemoryDS.prototype.confirm = function(hash) {
        console.log("MemoryDS:confirm", hash);
        return new Promise(function(resolve) {
            var affectedRecord = _.filter(this.data, function(d) {
                console.log("Analyzing operation %s to match %s", d.__op, hash);
                return d.__op === hash;
            }), toRemove = [];
            console.log("Found %d affected record(s) to confirm", affectedRecord.length);

            _.each(affectedRecord, function(record) {
                delete record.__temp;
                delete record.__op;

                /* istanbul ignore else */
                if(record.__removed) {
                    toRemove.push(record);
                }

                console.log("Confirmed record", record);
            });

            console.log("Found %d record to remove", toRemove.length);
            _.remove(this.data, function(record) { return _.contains(toRemove, record.hash) });

            resolve(true);
        }.bind(this));
    };

    Hermit.DataStoreClass = Hermit.MemoryDS = MemoryDS;

})(Hermit);
