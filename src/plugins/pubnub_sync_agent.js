(function(Hermit) {

    function PubNubSyncAgent(hermit, options) {
        var _this = this;

        console.log("PubNubSyncAgent:__constructor", options);
        this._hermit = hermit;
        this.options = options || {};
        this.connected = false;

        if(!window.PUBNUB) {
            throw new Error("Please make sure to add 'http://cdn.pubnub.com/pubnub.min.js' in your index.html page. ");
        }

        if(!options.subscribe_key) {
            throw new Error("Please add a 'sync_agent_cfg' entry to your configuration (in your app config hook) ");
        }

        EventEmitter2.call(this, {wildcard: true, maxListeners: 500});

        // Initializing pubnub
        this.pubnub = PUBNUB.init({
            publish_key   : options.publish_key,
            subscribe_key : options.subscribe_key
        });

        function onMessage(msg) {
            console.log("************* MESSAGE RECEIVED!");
            console.dir(msg);
            this.emit(msg.key, msg.payload);
        }

        // Subscribe to all required channels
        this.pubnub.subscribe({
            channel: [this._hermit.group || options.group, this._hermit.id, 'global' ],
            message: _.bind(onMessage, this),
            state: {
                device: this._hermit.id
            },
            restore: false,
            connect: function() {
                console.log("Device is connected to Pubnub network");
                _this.connected = true;
                _this.emit('connected');
            },
            disconnect: function() {
                console.log("Device is disconnected from the Pubnub network");
                _this.connected = false;
                _this.emit('disconnected');
            },
            reconnect: function() {
                console.log("Device was successfully reconnected to the Pubnub network");
                _this.connected = true;
                _this.emit('reconnected');
                _this.emit('connected');
            },
            error: function(err) {
                console.log("Unable to subscribe to channel [%s] in Pubnub network", [_this._hermit.group || options.group, _this._hermit.id, 'global' ].join(","), err);
                _this.connected = false;
                _this.emit('network-error', err);
            }
        });

    }

    PubNubSyncAgent.prototype = Object.create(EventEmitter2.prototype);
    PubNubSyncAgent.prototype.constructor = PubNubSyncAgent;

    /**
     * events are hierarchical.
     *
     * @param key Structure is : group.device.model.operation. Any element can be replaced by wildcards (** or *) to replace all or one subsequent element(s) ). The key can be password as a string
     *  or as an Array of strings containing each parts (easier than concatenating strings).
     * @param payload
     * @returns {*}
     */
    PubNubSyncAgent.prototype.send = function(key, payload) {
        return new Promise(function(resolve, reject) {
            console.log("PubNubSyncAgent:send", key, payload);

            // All outgoing messages are sent through the system channel. The platform will broadcast events on the right group channel if necessary
            this.pubnub.publish({
                channel: "system",
                message: {
                    key: key,
                    payload: payload,
                    device: this._hermit.id,
                    ts: Date.now()
                },
                callback: function() {
                    resolve(true);
                },
                error: function(err) {
                    console.log(err);
                    reject(err);
                }
            });
        }.bind(this));
    };

    /**
     *
     * @param keySpec A hash containing these elements : channel, device, model, operation or wildcards replacing any of the parts. Any missing element will be replaced by wildcard.
     *
     * @param handler
     * @param options
     */
    PubNubSyncAgent.prototype.register = function(keySpec, handler, options) {
        console.log("PubNubSyncAgent:register", keySpec);
        var _self = this, eventKey = [], opList;

        if(!keySpec) {
            throw new Hermit.Errors.InvalidSpec();
        }

        if(!keySpec.group) {
            console.log("No group, using wildcard");
            eventKey.push('*');
        }
        else
            eventKey.push(keySpec.group);
        if(!keySpec.device) {
            eventKey.push('*');
        }
        else
            eventKey.push(keySpec.device);
        if(!keySpec.model) {
            eventKey.push('*');
        }
        else
            eventKey.push(keySpec.model);
        if(!keySpec.operations) {
            opList = ['list', 'load', 'show', 'update', 'insert', 'remove', 'upsert'];
        }
        else {
            if(_.isArray(keySpec.operations)) {
                opList = keySpec.operations;
            }
            else {
                opList = keySpec.operations.split(',');
            }
        }

        console.log("Analyzed key:",eventKey," with operations", opList);

        options = options || {};

        _.each(opList, function(op) {
            console.log("Registering operation:", eventKey.concat(op));

            // Register our event handler
            _self.on(eventKey.concat(op), function(op) {
                var scope;

                console.log("Handling event:", this.event);

                // Special case for model scope
                if(options.scope === 'agent')
                    scope = _self;
                else if(options.scope)
                    scope = options.scope;
                else
                    scope = _self._hermit.model(keySpec.model);

                // Execute the handler in the provided scope (default to the agent itself)
                handler.call(scope || _self, op);
            });
        });

    };

    Hermit.SyncAgent = PubNubSyncAgent;

})(Hermit);
