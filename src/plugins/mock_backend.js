(function(Hermit) {

    function MockBackend(_hermit, options) {
        _.merge(this, options);
        this._hermit = _hermit;
    }

    MockBackend.prototype.setAuthorization = function(authorization) {
        this.headers.Authorization = authorization;
    };

    MockBackend.prototype.execute = function(op, options) {
        console.log("MockBackend:execute", op.uniqueKey(true));
        var defer = Promise.defer(), _self = this;
        _.delay(function() {
            var handler = _self[op.type];
            if(handler)
                defer.resolve(handler.call(_self, op, options));
            else {
                console.log("Unsupported operation", op.type);
                defer.resolve();
            }
        });
        return defer.promise;
    };

    MockBackend.prototype.list = function(op, options) {
        console.log("MockBackend:list", op.uniqueKey(true));
        return Promise.resolve([]);
    };

    MockBackend.prototype.show = function(op, options) {
        console.log("MockBackend:show",  op.uniqueKey(true));
        return Promise.resolve({key: op.key});
    };

    MockBackend.prototype.update = function(op, options) {
        console.log("MockBackend:update",  op.uniqueKey(true));
        var defer = Promise.defer(), _self = this;
        _.delay(function() {
            var obj = _.merge(op.data, { key: op.key});

            // Confirm this operation in our data store
            op.emit('commit');

            // Notify our group
            _self._hermit.getSyncAgent().send([op.model.group, '*', op.model.key, 'sync'], [op]);

            defer.resolve( obj );
        }, 0);

        return defer.promise;
    };

    MockBackend.prototype.insert = function(op, options) {
        console.log("MockBackend:insert",  op.uniqueKey(true));
        var defer = Promise.defer(), _self = this;
        _.delay(function() {
            var obj = op.data;

            // Confirm this operation in our data store
            op.emit('commit');

            // Notify our group
             _self._hermit.getSyncAgent().send([op.model.group, '*', op.model.key, 'sync'], [op]);

            defer.resolve( obj );
        }, 0);

        return defer.promise;
    };

    MockBackend.prototype.upsert = function(op, options) {
        console.log("MockBackend:upsert",  op.uniqueKey(true));
        var defer = Promise.defer(), _self = this;
        _.delay(function() {
            var obj = _.merge(op.data, { key: op.key});

            // Confirm this operation in our data store
            op.emit('commit');

            // Notify our group
            _self._hermit.getSyncAgent().send([op.model.group, '*', op.model.key, 'sync'], [op]);

            defer.resolve( obj );

        }, 0);
        return defer.promise;
    };

    MockBackend.prototype.remove = function(op, options) {
        console.log("MockBackend:remove",  op.uniqueKey(true));
        var defer = Promise.defer(), _self = this;
        _.delay(function() {

            // Confirm this operation in our data store
            op.emit('commit');

            // Notify our group
            _self._hermit.getSyncAgent().send([op.model.group, '*', op.model.key, 'sync'], [op]);

            defer.resolve( true );

        }, 0);
        return defer.promise;
    };

    Hermit.Backend = Hermit.MockBackend = MockBackend;

})(Hermit);
