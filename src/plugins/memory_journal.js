(function(Hermit) {

    function Journal(hermit, modelKey) {
        this.hermit = hermit;
        this.key = modelKey;
        this.queue = [];
        this.oplog = [];
    }

    Journal.prototype.recordOperation = function(op) {
        console.log("MemoryJournal:recordOperation", op.hash);
        return new Promise(function(resolve) {
            op.oplog_ts = Date.now();
            this.oplog.push(op);
            resolve(op);
        }.bind(this));
    };

    Journal.prototype.getLastOperation = function() {
        return new Promise(function(resolve) {
            var ops = _.sortBy(this.oplog, "oplog_ts").reverse();
            if(ops.length > 0)
                resolve(ops[0]);
            else
                resolve();
        }.bind(this));
    };

    Journal.prototype.clearQueue = function() {
        var _this = this;
        return new Promise(function(resolve) {
            _this.queue = [];
            resolve(true);
        });
    };

    Journal.prototype.enqueueOperation = function(op) {
        this.queue.push(op);
    };

    Journal.prototype.listQueuedOperations = function() {
        var _this = this;
        return new Promise(function(resolve) {
            resolve(_this.queue);
        });
    };

    Journal.prototype.getOperation = function(hash) {
        return new Promise(function(resolve) {
           return resolve(_.find(this.oplog, function(op){ return op.hash === hash }));
        }.bind(this));
    };

    Hermit.Journal = Journal;

})(Hermit);
