(function(Hermit) {

    function PouchCache() {
        if(!window.Hermit) {
            throw new Error("hermit.js must be loaded before this add-on");
        }


        if(!window.PouchDB) {
            throw new Error("Please add pouchdb before loading this add-on");
        }

    }

    Hermit.CacheClass = Hermit.PouchCache = PouchCache;

})(Hermit);
