(function (Hermit) {

    function RestBackend(_hermit, options) {
        options = options || {};
        _.merge(this, options);
        this._hermit = _hermit;
        this.headers = options.headers || {};

        // Create a journal to manage our outgoing operation queue
        this.queue = this._hermit.getJournal('rest_backend_queue');
        this.replay_throttle = this.replay_throttle || 5;
        this.timeout = options.timeout || 30000;

        console.log("Initialized a rest backend pointing to url ", this.baseUrl);

        // Register to receive notifications about the network state changes
        this._hermit.getNetworkStateManager().on('network-state-changed', function (networkState) {
            var _this = this;

            if (networkState.connected) {
                console.log("REST backend detected that the network is back. Let's replay queued operations");

                this.queue.listQueuedOperations().then(function (operations) {
                    console.log("Found %d operations to replay", operations.length);

                    Promise.map(operations, executeRestOperation.bind(this)).then(function (replayedOperations) {

                        return Promise.each(replayedOperations, function (op) {
                            if (op.result) {
                                console.log("Operation %s was successfully executed", op.hash);
                            }
                            else {
                                console.log("Operation %s has permanently failed", op.hash);
                            }

                            // Remove this operation from the queue
                            _this.queue.dequeueOperation(op);

                        }).then(function () {
                            console.log("All queued operations were processed");
                        });
                    }, {concurrency: _this.replay_throttle});

                }).catch(function (err) {
                    console.log("Unable to replay queue operations.", err);
                });
            }
        }.bind(this));
    }

    function executeRestOperation(op) {
        var _this = this;
        return new Promise(function (resolve) {

            if (window.JSONP && this._hermit.opts.backend_cfg && this._hermit.opts.backend_cfg.cors && this._hermit.opts.backend_cfg.cors.jsonp) {
                console.log("Executing JSONP %s %s", op.method, this.baseUrl + op.path);

                JSONP.get(this.baseUrl + op.path, op.data, function (response) {
                    if (_this._hermit.opts.backend_cfg.normalize) {
                        var result = _this._hermit.opts.backend_cfg.normalize(response, op);
                        if (op.deferredResult)
                            op.deferredResult.resolve(result);
                        op.result = result;

                        // Apply all mapping in reverse on result
                        applyResponseMappings(op, _this._hermit.opts.backend_cfg ? _this._hermit.opts.backend_cfg.mappings: {});

                        resolve(op);
                    }
                    /* istanbul ignore else */
                    else if (op.deferredResult) {
                        op.deferredResult.resolve(response);
                        op.result = response;

                        // Apply all mapping in reverse on result
                        applyResponseMappings(op, _this._hermit.opts.backend_cfg ? _this._hermit.opts.backend_cfg.mappings: {});

                        resolve(op);
                    }
                    else {
                        console.log("No waiting deferred. Result of this operation will be lost");
                        op.result = response;

                        // Apply all mapping in reverse on result
                        applyResponseMappings(op, _this._hermit.opts.backend_cfg ? _this._hermit.opts.backend_cfg.mappings: {});

                        resolve(op);
                    }
                });

            }
            else {
                console.log("Executing AJAX %s %s", op.method, this.baseUrl + op.path);

                ajax(this.baseUrl + op.path, {
                    method: op.method,
                    headers: op.headers,
                    data: op.data,
                    success: function (response) {
                        console.log("Successfully executed the backend request", response);

                        if (_this._hermit.opts.backend_cfg && _this._hermit.opts.backend_cfg.normalize) {
                            console.log("Normalizing result");

                            var result = _this._hermit.opts.backend_cfg.normalize(JSON.parse(response), op);

                            op.result = result;

                            // Apply all mapping in reverse on result
                            applyResponseMappings(op, _this._hermit.opts.backend_cfg ? _this._hermit.opts.backend_cfg.mappings: {});

                            /* istanbul ignore else */
                            if (op.deferredResult) {
                                console.log("Resolving the deferredResult of operation %s", op.id);
                                op.deferredResult.resolve(op.result);
                            }

                            resolve(op);
                        }
                        else if (op.deferredResult) {
                            console.log("Resolving the operation deferredResult");

                            op.deferredResult.resolve(JSON.parse(response));
                            op.result = JSON.parse(response);

                            // Apply all mapping in reverse on result
                            applyResponseMappings(op, _this._hermit.opts.backend_cfg ? _this._hermit.opts.backend_cfg.mappings: {});

                            resolve(op);
                        }
                        else {
                            console.log("No waiting deferred for operation %s. Result of this operation will be lost", op.hash);
                            op.result = JSON.parse(response);

                            // Apply all mapping in reverse on result
                            applyResponseMappings(op, _this._hermit.opts.backend_cfg ? _this._hermit.opts.backend_cfg.mappings: {});

                            resolve(op);
                        }
                    },
                    error: function (xhr) {
                        console.log("HTTP Backend Error: ", xhr);
                        /* istanbul ignore else */
                        if (op.deferredResult) {
                            console.log("rejecting the deferred result of the operation");
                            op.deferredResult.reject({status: xhr.status, msg: xhr.statusText});
                        }
                        else {
                            console.log("Unhandled XHR error for operation %s", op.hash, {
                                status: xhr.status,
                                msg: xhr.statusText
                            });
                        }
                        op.error = {status: xhr.status, msg: xhr.statusText};
                        resolve(op);
                    }
                });
            }
        }.bind(this));
    }

    function applyMappings(op, mappings) {
        if(op.data) {
            op.data = _.omit(op.data, "__temp", "__op", "__removed", "$temp", "$op", "$removed");

            if (op.model.backend_cfg) {
                mappings = _.merge(mappings || {}, op.model.backend_cfg.mappings);
            }

            if (mappings) {
                if (_.isFunction(mappings)) {
                    op.data = mappings(op.data);
                }
                else {
                    _.each(_.keys(mappings), function (fieldName) {
                        if (!mappings[fieldName]) {
                            delete op.data[fieldName];
                        }
                        else if (_.isString(mappings[fieldName])) {

                            if(mappings[fieldName][0] === '.') {
                                if(op.data[fieldName])
                                    op.data[mappings[fieldName]] = op.data[fieldName][mappings[fieldName].substring(1)];
                            }
                            else {
                                op.data[mappings[fieldName]] = op.data[fieldName];
                            }
                            delete op.data[fieldName];
                        }
                    });
                }
            }
        }
        return op.data;
    }

    function applyResponseMappings(op, mappings) {
        if(op.result) {
            op.result = _.omit(op.result, "__temp", "__op", "__removed", "$temp", "$op", "$removed");
            op.result = _.omit(op.result, "__temp", "__op", "__removed", "$temp", "$op", "$removed");

            if (op.model && op.model.backend_cfg) {
                mappings = _.merge(mappings || {}, op.model.backend_cfg.mappings);
            }

            if (mappings) {
                if (_.isFunction(mappings)) {
                    // Call the mapping function with response flag to true
                    op.result = mappings(op.result, true);
                }
                else {
                    // Loop through all mappings (reverse only strings)
                    _.each(_.keys(mappings), function (fieldName) {
                        if (_.isString(mappings[fieldName])) {
                            if(mappings[fieldName][0] !== '.') {
                                op.result[fieldName] = op.result[mappings[fieldName]];
                                delete op.result[mappings[fieldName]];
                            }
                        }
                    });
                }
            }
        }
        return op.result;
    }

    function commitAndSync(op, result) {
        var _this = this;

        // Confirm this operation in our data store
        console.log("Committing and publishing operation %s", op.id);

        op.emit('commit');

        // Notify our group
        this._hermit.getConnectedSyncAgent().then(function (syncAgent) {
            console.dir(result);
            var operations = Hermit.Operation.plain(op, result);
            console.dir(operations);
            _.each(operations, function(op) {
                applyMappings(op, _this._hermit.opts.backend_cfg ? _this._hermit.opts.backend_cfg.mappings : {});
            });
            console.dir(operations);
            syncAgent.send([op.model.group, _this._hermit.id, op.model.key, 'publish'], operations, {token: _this.headers.Authorization});
        });

        return result;
    }

    function restOperation(method, path, data, options, op) {
        console.log("RestBackend:" + method, path);
        var defer = Promise.defer(), _this = this;

        // Some operations don't have data
        if (arguments.length === 4) {
            op = options;
            options = data;
            data = undefined;
        }

        // Append all query parameters to url
        if (_.isEmpty(options.query)) {
            console.log("Detected a query string to build", options.query);
            path += "?";

            _.each(_.keys(options.query), function (key) {
                if (options.query[key]) {
                    path += key + "=" + encodeURIComponent(options.query[key]) + "&";
                }
                else
                    path += key + "=" + "&";
            });
        }

        /* istanbul ignore else */
        if (options.hash) {
            if (path.indexOf("&") !== -1) {
                path += "h=" + options.hash;
            }
            else
                path += "?h=" + options.hash;
        }

        if(options.headers) {

            if(!options.headers['Content-Type']) {
                options.headers['Content-Type'] = 'application/json';
            }

        }

        /* istanbul ignore else */
        if (data) {
            data = applyMappings(op, _this._hermit.opts.backend_cfg ? _this._hermit.opts.backend_cfg.mappings : {});
        }

        var restOp = {
            id: options.id,
            hash: options.hash,
            method: method,
            path: path,
            headers: _.merge(this.headers, options.headers || {'Content-Type': 'application/json'}),
            data: data ? JSON.stringify(data) : undefined,
            deferredResult: defer,
            model: op.model
        };

        // Execute the operation on the backend if we have a network
        if (this._hermit.getNetworkStateManager().isNetworkConnected()) {
            executeRestOperation.call(this, restOp).catch(function(err) {
                console.log("Unable to execute backend operation %s.", restOp.id, err);
            });
        }
        else {
            console.log("Queueing operation %s as no network is available.", restOp.id);

            // Queue the operation in case of temporary network shortage
            this.queue.enqueueOperation(restOp);
        }

        return defer.promise;
    }

    function getBackendModelKey(op) {
        console.log("RestBackend:getBackendModelKey");

        if (!op.model)
            throw new Hermit.Errors.InvalidConfiguration();

        if (op.model.backend_cfg) {
            console.log("Checking for overriden backend key", op.model.backend_cfg);
            // Allow for backend collection names override
            if (op.model.backend_cfg.key) {
                console.log("We have an overriden backend key", op.model.backend_cfg.key);
                return op.model.backend_cfg.key;
            }
        }
        else {
            console.log("Using the declared model key", op.model.key);
        }

        return op.model.key;
    }

    function getSubTypeUrl(op) {
        var extra = "";

        if (op.subType)
            extra += "/" + op.subType;

        if (op._hermit.opts.backend_cfg && (op._hermit.opts.backend_cfg.extension || op.extension)) {
            extra += (op._hermit.opts.backend_cfg.extension || op.extension);
        }

        // Handle sub item if specified
        if (op.subItem) {
            // Sub item may be a variable name, in this case, the value must be in the operation
            if (op.subItem.indexOf(':') === 0)
                extra += '/' + op[op.subItem.substring(1)];
            else
                extra += '/' + op.subItem;
        }

        return extra;
    }

    RestBackend.prototype.setAuthorization = function (authorization) {
        this.headers.Authorization = authorization;
    };

    RestBackend.prototype.execute = function (op, options) {
        var handler = this[op.type];
        if (handler)
            return handler.call(this, op, options);
        else
            throw new Hermit.Errors.InvalidOperation(op.type);
    };

    RestBackend.prototype.list = function (op, options) {
        options = _.merge(options || {}, {id: op.id, hash: op.hash});

        // Make sure we use default query parameters in all operations
        if (this._hermit.opts.backend_cfg) {
            options.query = _.merge(options.query || {}, this._hermit.opts.backend_cfg.query);
            options.headers = _.merge(options.headers || {}, this._hermit.opts.backend_cfg.headers);
            options.mappings = this._hermit.opts.backend_cfg.mappings;
        }

        if (op.model.backend_cfg) {
            options.mappings = _.merge(options.mappings || {}, op.model.backend_cfg.mappings);
        }

        return restOperation.bind(this)("GET", "/" + getBackendModelKey(op) + getSubTypeUrl(op), _.merge(options, {query: op.query}), op);
    };

    RestBackend.prototype.load = function (op, options) {
        options = _.merge(options || {}, {id: op.id, hash: op.hash});

        // Make sure we use default query parameters in all operations
        if (this._hermit.opts.backend_cfg) {
            options.query = _.merge(options.query || {}, this._hermit.opts.backend_cfg.query);
            options.headers = _.merge(options.headers || {}, this._hermit.opts.backend_cfg.headers);
            options.mappings = this._hermit.opts.backend_cfg.mappings;
        }

        if (op.model.backend_cfg) {
            options.mappings = _.merge(options.mappings || {}, op.model.backend_cfg.mappings);
        }

        return restOperation.bind(this)("GET", "/" + getBackendModelKey(op) + getSubTypeUrl(op), _.merge(options, {
            query: {
                filterExpr: op.options.filter,
                load: true,
                preflight: options.preflight
            }
        }), op);
    };

    RestBackend.prototype.show = function (op, options) {
        options = _.merge(options || {}, {id: op.id, hash: op.hash});

        // Make sure we use default query parameters in all operations
        if (this._hermit.opts.backend_cfg) {
            options.query = _.merge(options.query || {}, this._hermit.opts.backend_cfg.query);
            options.headers = _.merge(options.headers || {}, this._hermit.opts.backend_cfg.headers);
            options.mappings = this._hermit.opts.backend_cfg.mappings;
        }

        if (op.model.backend_cfg) {
            options.mappings = _.merge(options.mappings || {}, op.model.backend_cfg.mappings);
        }

        return restOperation.bind(this)("GET", "/" + getBackendModelKey(op) + "/" +  encodeURIComponent(op.key) + getSubTypeUrl(op), _.merge(options, {query: op.query}), op);
    };

    RestBackend.prototype.update = function (op, options) {
        options = _.merge(options || {}, {id: op.id, hash: op.hash});

        console.log("Operation target key is ", op.key);

        // Make sure we use default query parameters in all operations
        if (this._hermit.opts.backend_cfg) {
            options.query = _.merge(options.query || {}, this._hermit.opts.backend_cfg.query);
            options.headers = _.merge(options.headers || {}, this._hermit.opts.backend_cfg.headers);
            options.mappings = this._hermit.opts.backend_cfg.mappings;
        }

        if (op.model.backend_cfg) {
            options.mappings = _.merge(options.mappings || {}, op.model.backend_cfg.mappings);
        }

        return restOperation.bind(this)("PUT", "/" + getBackendModelKey(op) + "/" +  encodeURIComponent(op.key) + getSubTypeUrl(op), op.data, _.merge(options, {
            hash: op.hash,
            query: op.query
        }), op).then(_.bind(commitAndSync, this, op));
    };

    RestBackend.prototype.insert = function (op, options) {
        options = _.merge(options || {}, {id: op.id, hash: op.hash});

        // Make sure we use default query parameters in all operations
        if (this._hermit.opts.backend_cfg) {
            options.query = _.merge(options.query || {}, this._hermit.opts.backend_cfg.query);
            options.headers = _.merge(options.headers || {}, this._hermit.opts.backend_cfg.headers);
        }

        if (op.model.backend_cfg) {
            options.mappings = _.merge(options.mappings || {}, op.model.backend_cfg.mappings);
        }

        return restOperation.bind(this)("POST", "/" + getBackendModelKey(op) + getSubTypeUrl(op), op.data, _.merge(options, {
            hash: op.hash,
            query: op.query
        }), op).then(_.bind(commitAndSync, this, op));
    };

    RestBackend.prototype.remove = function (op, options) {
        options = _.merge(options || {}, {id: op.id, hash: op.hash});

        // Make sure we use default query parameters in all operations
        if (this._hermit.opts.backend_cfg) {
            options.query = _.merge(options.query || {}, this._hermit.opts.backend_cfg.query);
            options.headers = _.merge(options.headers || {}, this._hermit.opts.backend_cfg.headers);
            options.mappings = this._hermit.opts.backend_cfg.mappings;
        }

        if (op.model.backend_cfg) {
            options.mappings = _.merge(options.mappings || {}, op.model.backend_cfg.mappings);
        }

        return restOperation.bind(this)("DELETE", "/" + getBackendModelKey(op) + "/" + encodeURIComponent(op.key) + getSubTypeUrl(op), _.merge(options, {
            hash: op.hash,
            query: op.query
        }), op).then(_.bind(commitAndSync, this, op));
    };

    RestBackend.prototype.upsert = function (op, options) {
        options = _.merge(options || {}, {id: op.id, hash: op.hash});

        // Make sure we use default query parameters in all operations
        if (this._hermit.opts.backend_cfg) {
            options.query = _.merge(options.query || {}, this._hermit.opts.backend_cfg.query);
            options.headers = _.merge(options.headers || {}, this._hermit.opts.backend_cfg.headers);
            options.mappings = this._hermit.opts.backend_cfg.mappings;
        }

        if (op.model.backend_cfg) {
            options.mappings = _.merge(options.mappings || {}, op.model.backend_cfg.mappings);
        }

        return restOperation.bind(this)("POST", "/" + getBackendModelKey(op) + "/" + encodeURIComponent(op.key) + getSubTypeUrl(op), op.data, _.merge(options, {
            hash: op.hash,
            query: op.query
        }), op).then(_.bind(commitAndSync, this, op));
    };

    Hermit.Backend = Hermit.RestBackend = RestBackend;

})(Hermit);
