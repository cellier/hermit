(function(Hermit) {

    function FirebaseDS() {
        if(!window.Hermit) {
            throw new Error("hermit.js must be loaded before this add-on");
        }


        if(!window.Firebase) {
            throw new Error("Please add firebase before loading this add-on");
        }

    }

    FirebaseDS.prototype.list = function() {

    };

    FirebaseDS.prototype.show = function() {

    };

    FirebaseDS.prototype.insert = function() {

    };

    FirebaseDS.prototype.update = function() {

    };

    FirebaseDS.prototype.remove = function() {

    };

    FirebaseDS.prototype.upsert = function() {

    };

    Hermit.DataStoreClass = Hermit.FirebaseDS = FirebaseDS;

})(Hermit);
