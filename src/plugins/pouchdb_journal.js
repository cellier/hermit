(function(Hermit) {

    function Journal(hermit, modelKey, options) {
        options = options || {};
        this._hermit = hermit;
        this.key = modelKey;
        this.db_name = this._hermit.id + "_" + (options.namespace || "") + "_" + modelKey;
        this.activeOps = {};

        /* istanbul ignore next */
        if (!window.Hermit) {
            throw new Error("hermit.js must be loaded before this add-on");
        }

        /* istanbul ignore next */
        if (!window.PouchDB) {
            throw new Error("Please add pouchdb before loading this add-on");
        }

        this.queue = new PouchDB(this.db_name + "_queue");
        this.oplog = new PouchDB(this.db_name + "_oplog");
    }

    Journal.prototype.clearQueue = function() {
        var _this = this;
        return new Promise(function(resolve, reject) {
            _this.queue.destroy(function(err) {
                if(err) {
                    reject(err);
                }
                else {
                    _this.queue = new PouchDB(_this.db_name + "_queue");
                    _this.activeOps = {};
                    resolve(true);
                }
            });
        });
    };

    Journal.prototype.listQueuedOperations = function() {
        return new Promise(function(resolve, reject) {
            var _this = this;
            this.queue.allDocs({include_docs: true}, function(err, result) {
                if(err) {
                    reject(err);
                }
                else {
                    resolve(_.sortBy(_.map(result.rows, function(row) {
                        var op = row.doc;
                        // Restore the deferred Result if operation is still active
                        op.deferredResult = _this.activeOps[op.id];
                        return op;
                    }), function(op){ return op.ts }));
                }
            });
        }.bind(this));
    };

    Journal.prototype.dequeueOperation = function(op) {
        return new Promise(function(resolve, reject) {
            var _this = this;
            this.queue.remove(op.id, function(err) {
                if(err) {
                    reject(err);
                }
                else {
                    delete _this.activeOps[op.id];
                    resolve(true);
                }
            });
        }.bind(this));
    };

    /**
     * Enqueue operations that have to be applied to the server when network goes up
     */
    Journal.prototype.enqueueOperation = function(op) {
        return new Promise(function(resolve, reject) {

            // Make sure we store a plain operation
            var persist_op = Hermit.Operations.plain(op);

            persist_op._id = op.id;
            persist_op.ts = Date.now();

            if(op.deferredResult) {
                this.activeOps[op.id] = op.deferredResult;
                delete persist_op.deferredResult;
            }

            this.queue.put(persist_op, function(err) {
                if(err) {
                    reject(err);
                }
                else {
                    resolve(op);
                }
            });

        }.bind(this));
    };

    /**
     * We keep a log of all applied operations to avoid executing it twice.
     * @param op
     */
    Journal.prototype.recordOperation = function(ops) {
        var _this = this;
        var operations = Hermit.Operation.plain(ops);

        return Promise.map(operations, function(op) {
            console.log("Recording operation", op);

            op.oplog_ts = Date.now();
            op._id = op.id;

            return _this.oplog.put(op).then(function() {
                // Remove this operation from the queue (if there)
                _this.queue.remove(op.id).catch(function() {
                    // NOOP - The document might not be there, we just want to clean things up here.
                    console.log("Operation wasn't in the queue");
                });
            });

        }).catch(function(err) {
            console.log(err);
        });
    };

    Journal.prototype.getLastOperation = function() {
        return this.oplog.allDocs({descending: true, reduce: false, include_docs: true}).then(function(result) {
            if(result.rows.length > 0) {
                return result.rows[0].doc;
            }
        });
    };

    Journal.prototype.getOperation = function(id) {
        // Avoid throwing exception if operation isn't found. This is a somewhat crazy PouchDB pattern...
        return this.oplog.get(id).then(function(op) {
            return op;
        }, function(err) {
            // NOOP
        });
    };

    Hermit.Journal = Hermit.PouchDBJournal = Journal;

})(Hermit);
