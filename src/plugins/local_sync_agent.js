(function (Hermit) {

    function LSA(hermit, options) {
        var _self = this;
        console.log("LSA:__constructor");
        this._hermit = hermit;
        this.options = options || {};
        EventEmitter2.call(this, {wildcard: true, maxListeners: 500});
        this.connected = true;

        // Connect our sync-request handler
        // NOTE: We're simulating a remote sync handler here... This is only for TEST!
        this.on('*.*.*.sync', function (syncRequest) {

            // Local sync agent received messages from multiple channels, which is not the case for the real syncAgent, which will subscribe to the system channel only, not the device
            // or group messages
            if (syncRequest._sync_agent && syncRequest._sync_agent.channel === 'system') {
                console.log("Processing sync request for model %s on device %s", syncRequest.model, this.event[1]);

                var op = _self._hermit.newOperation({type: 'load', model: {key: syncRequest.model}});

                var operations = [];
                operations.push({
                    hash: op.hash,
                    group: this.event[1],
                    device: this.event[1],
                    type: 'load',
                    stereotype: {type: String},
                    key: op.key,
                    data: op.data,
                    from_hash: op.from_hash,
                    created_at: Date.now()
                });
//                this.emit([_self.event[2], _self.event[1], _self.event[2], _self.event[3], 'sync'], operations);
            }
            else
                console.log("Skipping a non system message");
        });
    }

    LSA.prototype = Object.create(EventEmitter2.prototype);
    LSA.prototype.constructor = LSA;

    LSA.prototype.send = function (key, payload, options) {
        options = options || {};

        payload._sync_agent = {
            channel: options.channel || 'system'
        };

        this.emit(key, payload);
        return Promise.resolve(true);
    };

    /**
     *
     * @param keySpec A hash containing these elements : channel, device, model, operation or wildcards replacing any of the parts. Any missing element will be replaced by wildcard.
     *
     * @param handler
     * @param options
     */
    LSA.prototype.register = function (keySpec, handler, options) {
        console.log("LSA:register", keySpec);
        var _self = this, eventKey = [], opList;

        if (!keySpec) {
            throw new Hermit.Errors.InvalidSpec();
        }

        if (!keySpec.group) {
            console.log("No group, using wildcard");
            eventKey.push('*');
        }
        else
            eventKey.push(keySpec.group);
        if (!keySpec.device) {
            eventKey.push('*');
        }
        else
            eventKey.push(keySpec.device);
        if (!keySpec.model) {
            eventKey.push('*');
        }
        else
            eventKey.push(keySpec.model);
        if (!keySpec.operations) {
            opList = ['list', 'show', 'update', 'insert', 'remove', 'upsert'];
        }
        else {
            if (_.isArray(keySpec.operations)) {
                opList = keySpec.operations;
            }
            else {
                opList = keySpec.operations.split(',');
            }
        }

        console.log("Analyzed key:%s with operations %s", eventKey, opList);

        options = options || {};

        _.each(opList, function (op) {
            console.log("Registering operation %s", eventKey.concat(op));

            // Register our event handler
            _self.on(eventKey.concat(op), function (op) {
                var scope;

                // Special case for model scope
                if (options.scope === 'agent')
                    scope = _self;
                else if (options.scope)
                    scope = options.scope;
                else
                    scope = _self._hermit.model(keySpec.model);

                // Execute the handler in the provided scope (default to the agent itself)
                handler.call(scope || _self, op);
            });
        });

    };

    Hermit.SyncAgent = Hermit.LocalSyncAgent = LSA;

})(Hermit);
