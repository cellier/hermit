(function(Hermit) {

    function DNM() {
        var _self = this;
        this.networkState = {
            connected: false
        };

        /* istanbul ignore if */
        if(window.cordova) {

            // Setup cordova network plugin event handlers
            document.addEventListener("offline", function() {
                _self.setNetworkConnected(false);
            });

            /* istanbul ignore next */
            document.addEventListener("online", function() {
                _self.setNetworkConnected(true);
            });
        }

        EventEmitter2.call(this, { maxListeners: 500 });

    }

    DNM.prototype = Object.create(EventEmitter2.prototype);
    DNM.prototype.constructor = DNM;

    DNM.prototype.setNetworkConnected = function(connected) {
        console.log("DefaultNetworkStateManager:setNetworkConnected", connected);
        this.networkState.connected = connected;
        this.emit('network-state-changed', this.networkState);
    };

    DNM.prototype.isNetworkConnected = function() {
        console.log("DefaultNetworkStateManager:isNetworkConnected", this.networkState.connected);
        return this.networkState.connected;
    };

    Hermit.DefaultNetworkStateManager = DNM;

})(Hermit);
