(function(exports) {

    var instances = {};

    Promise.longStackTraces();

    function Hermit(opts) {
        console.log("Initializing Hermit instance");

        opts = this.opts = opts || {};

        this.baseUrl = opts.baseUrl || location.origin;
        this.caches = {};
        this.journals = {};

        // Register this hermit instance
        this.id = opts.id;
        if(this.id) {
            console.log("Registering hermit instance", opts.id);
            instances[this.id] = this;
            console.log("Instance is registered");
        }
        else if(opts.device && opts.device.id) {
            this.id = opts.device.id;
            instances[this.id] = this;
        }
        else {
            console.log("No id was provided for this instance. It will not be managed");
        }

        // Assign the group for this hermit instance (or the instance id if no group is specified)
        if(opts.group) {
            this.group = opts.group;
        }
        else
            this.group = this.id;

        // Initialize collaborators
        /* jshint -W056 */
        var modelProvider = new (opts.modelProvider || Hermit.DefaultModelProvider)(this, opts.model_provider_cfg);
        var networkStateMgr = new (opts.networkStateManager || Hermit.DefaultNetworkStateManager)(this, opts.network_state_mgr_cfg);
        var operationFactory = new (opts.operationFactory || Hermit.OperationFactory)(this, opts.operation_factory_cfg);
        var dataStoreFactory = new (opts.dsFactory || Hermit.DataStoreFactory)(this, opts.ds_factory_cfg);
        var cacheProvider = new (opts.cacheProvider || Hermit.DefaultCacheProvider)(this, opts.cache_provider_cfg);

        this.getModelProvider = function() {
            return modelProvider;
        };

        this.getNetworkStateManager = function() {
            return networkStateMgr;
        };

        this.getOperationFactory = function() {
            return operationFactory;
        };

        this.getDataStoreFactory = function() {
            return dataStoreFactory;
        };

        this.getCache = function(cacheKey) {
            return cacheProvider.getCache(cacheKey);
        };

        EventEmitter2.call(this);

    }

    Hermit.prototype = Object.create(EventEmitter2.prototype);
    Hermit.prototype.constructor = Hermit;

    Hermit.prototype.model = function(key) {
        return this.getModelProvider().getModel(key);
    };

    Hermit.prototype.newOperation = function(spec) {
        return this.getOperationFactory().newOperation(spec);
    };


    Hermit.prototype.wrapOperation = function(spec) {
        return this.getOperationFactory().wrap(spec);
    };

    Hermit.prototype.DS = function(key, options) {
        return this.getDataStoreFactory().openDS(this.id, key, options);
    };

    Hermit.prototype.getBackend = function() {

        if(!this.backend) {

            if(!Hermit.Backend) {
                throw new Hermit.Errors.InvalidConfiguration("missing.backend.impl");
            }

            console.log("Creating backend instance with baseUrl : ", this.baseUrl);
            this.backend = new Hermit.Backend(this, _.merge({ baseUrl: this.baseUrl }, this.opts.backend_cfg || {} ));

        }

        return this.backend;
    };

    Hermit.prototype.getJournal = function(modelKey) {
        var journal = this.journals[modelKey];
        if(!journal) {
            this.journals[modelKey] = journal = new Hermit.Journal(this, modelKey, this.journal_cfg);
        }
        return journal;
    };

    Hermit.prototype.getSyncAgent = function() {
        // Lazy initialization of the sync agent
        if(!this.syncAgent) {
            if(!Hermit.SyncAgent) {
                throw new Hermit.Errors.InvalidConfiguration("missing.syncagent.impl");
            }

            console.log("Creating syncAgent instance");
            this.syncAgent = new Hermit.SyncAgent(this, this.opts.sync_agent_cfg);
        }
        return this.syncAgent;
    };

    Hermit.prototype.getConnectedSyncAgent = function() {
        var _this = this;
        return new Promise(function(resolve) {
            var syncAgent = _this.getSyncAgent();
            if(syncAgent.connected) {
                resolve(syncAgent);
            }
            else {
                syncAgent.once('connected', function() {
                    resolve(syncAgent);
                });
            }
        });
    };

    Hermit.prototype.sync = function(options) {
        var _this = this;
        console.log("Hermit:sync", options);
        return new Promise(function(resolve, reject) {
            var deferred = this;

            if(_this.getNetworkStateManager().isNetworkConnected() ) {
                var modelProvider = _this.getModelProvider();
                resolve(Promise.all(modelProvider.visitModels(function(model) {
                    console.log("Model Visitor was called for model", model.key);
                    return model.manual_sync(options);
                })));
            }
            else {
                console.log("Cannot sync, network is unavailable. Sync will be performed next time network is detected");
                reject({ error: true, code: 'no-network'});
            }
        });
    };

    Hermit.getInstance = function(id) {
        var hermit;
        if(id) {
            console.log("Retrieving instance", id);
            hermit = instances[id];
        }
        else {
            console.log("No ID was specified, retrieving the first instance");
            var hermits = _.values(instances);
            if(hermits.length > 0)
                hermit = hermits[0];
        }

        return hermit;
    };

    Hermit.reset = function() {
        instances = {};
    };

    function customError(errors, name) {
        errors[name] = function() {
            Error.apply(this, arguments);
        };
        errors[name].prototype = Object.create(Error.prototype);
        errors[name].prototype.constructor = errors[name];
    }

    // Declare a few error classes
    Hermit.Errors = {};

    customError(Hermit.Errors, "InvalidSpec");
    customError(Hermit.Errors, 'InvalidOperation');
    customError(Hermit.Errors, 'InvalidConfiguration');
    customError(Hermit.Errors, 'InvalidParameter');
    customError(Hermit.Errors, 'OperationUnavailable');
    customError(Hermit.Errors, "SyncError");

    exports.Hermit = Hermit;


})( ( /*jshint ignore:start */
    typeof exports === 'undefined' ? this : exports
    /* jshint ignore:end */)
);


