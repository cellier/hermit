(function(Hermit) {

    Hermit.Operations.show = function() {
        console.log("------------------------------------------");
        console.log("ShowOperation:execute", this.key);
        var defer = Promise.defer(), _self = this;

        var cacheSpec = this.cache || this.model.cache;

        this.once('execute-backend-op', function() {
            console.log("Executing operation %s on backend", _self.hash);

            // Is the network up?
            if(_self._hermit.getNetworkStateManager().isNetworkConnected()) {
                console.log("network is available. Let's execute our backend operation");

                // Execute the operation on the backend
                defer.resolve(_self._hermit.getBackend().execute(_self).then(function(result){
                    console.log("Received result from backend:", result);

                    // update the local cache if supported
                    if(cacheSpec) {
                        _.delay(function() {
                            _self.emit('cache-result', result);
                        });
                    }

                    return result;
                }));
            }
            else {
                //TODO: We could perform retries here... Until we reach a timeout value or a number of times?
                console.log("Nothing is available... we fail!");
                // We fail... Cannot do a list without cache, datastore or network!
                defer.reject(new Hermit.Errors.OperationUnavailable());
            }
        });

        this.once('load-from-local-ds', function() {
            console.log("Delegating show operation to DS");
            _self.model.ds.then(function(ds) {
                if(ds.synced(_self.model.max_sync_delay)) {
                    console.log("DS is synced enough.. let's retrieve data");
                    defer.resolve(ds.show(_self.key, _self.options).then(function(result) {

                        // update the local cache if supported
                        if(_self.model.cache) {
                            _self.emit('cache-result', result);
                        }
                        return result;
                    }));
                }
                else {
                    // Let's get to the backend if we don't have a valid local copy
                    _self.emit('execute-backend-op');
                }
            });

        });

        // We need to update the cache
        this.once('cache-result', function(result) {
            var cache = _self._hermit.getCache(cacheSpec.key || _self.model.key);

            // Update the cache entry for this key with the new results
            cache.setEntry(_self.hash, result, { ikey: _self.uniqueKey() });
        });

        // Do we maintain a cache for this model?
        if(cacheSpec) {

            // Retrieve the result for this operation in our cache.
            _self._hermit.getCache(cacheSpec.key || this.model.key).getEntry(this).then(function(result) {
                if(result) {
                    console.log("Cache hit for operation: %s. Quickly returning result", _self.hash);
                    defer.resolve(result.data);
                    return result.data;
                }
                else if(_self.model.ds && !_self.serverOnly) {
                    console.log("Nothing in cache, looking in local datastore");
                    _self.emit('load-from-local-ds');
                }
                else {
                    console.log("Nothing in cache and no local ds, go directly to backend");
                    _self.emit('execute-backend-op');
                }

            }).catch(function(err) {
                defer.reject(err);
            });

        }
        else {

            // Do we have an up to date local DS?
            if(_self.model.ds && !_self.serverOnly) {
                _self.emit('load-from-local-ds');
            }
            else {
                _self.emit('execute-backend-op');
            }
        }

        return defer.promise;
    };

})(Hermit);