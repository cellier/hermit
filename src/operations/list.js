(function(Hermit){

    Hermit.Operations.list = function() {
        console.log("------------------------------------------");
        console.log("ListOperation:execute");
        return new Promise(function(resolve, reject) {
            var _self = this;

            //NOTE: We use internal events to avoid complex branching and duplicated code
            this.once('execute-backend-op', function() {
                console.log("Executing operation %s on backend", _self.uniqueKey(true));

                // Is the network up?
                if(_self._hermit.getNetworkStateManager().isNetworkConnected()) {
                    console.log("network is available. Let's execute our backend operation");

                    // Execute the operation on the backend
                    resolve(_self._hermit.getBackend().execute(_self).then(function(result) {
                        console.log("Received result from backend:", result);

                        // Reinitialize the datastore if that's a loading operation
                        if(_self.model.ds && _self.initialize) {
                            _self.emit('initialize-ds', result);
                        }

                        // update the local cache if supported
                        if(_self.model.cache) {
                            _self.emit('cache-result', result);
                        }

                        return result;
                    }));
                }
                else {
                    //TODO: We could peform retries here... Until we reach a timeout value or a number of times?
                    console.log("Nothing is available to list... we fail!");
                    // We fail... Cannot do a list without cache, datastore or network!
                    reject(new Hermit.Errors.OperationUnavailable());
                }
            });

            this.once('load-from-local-ds', function() {
                console.log("Delegating list operation to DS");
                _self.model.ds.then(function(ds) {
                    console.log("Loaded model datastore");
                    if(ds.synced(_self.model.max_sync_delay)) {
                        console.log("DS is synced enough.. let's retrieve data");
                        resolve(ds.list(_self.options).then(function(result) {

                            // update the local cache if supported
                            if(_self.model.cache) {
                                _self.emit('cache-result', result);
                            }
                            return result;
                        }));
                    }
                    else {
                        console.log("DS is not in-sync, delegating to the backend");
                        // Let's get to the backend if we don't have a valid local copy
                        _self.emit('execute-backend-op');
                    }
                });

            });

            this.once('initialize-ds', function(data) {

                _self.model.ds.then(function(ds) {

                    // Replace the database with our new data
                    return ds.reset(_self.options).then(function() {
                        console.log("Internal PouchDB was successfully reset");
                        return ds.insert(data, _self.options);
                    });

                }).then(function(inserted) {
                    console.log("Datastore for model %s was successfully (re)initialized", _self.model.key);
                    return inserted;
                }).catch(function(err) {
                    console.log("Unable to reinitialize datastore model %s", _self.model.key, err);
                    reject(err);
                });
            });

            // We need to update the cache
            this.once('cache-result', function(result) {
                var cache = _self._hermit.getCache(this.model.cache.key || this.model.key);

                // Update the cache entry for this key with the new results
                cache.setEntry(this.hash, result, { ikey: this.uniqueKey() });
            });

            // Do we maintain a cache for this model?
            if(this.model.cache) {
                console.log("Model supports cache, let's look for a hit!");

                // Retrieve the result for this operation in our cache.
                _self._hermit.getCache(this.model.cache.key || this.model.key).getEntry(this).then(function(result) {
                    if(result) {
                        console.log("Cache hit for operation: %s. Quickly returning result", _self.hash);
                        resolve(result.data);
                        return result.data;
                    }
                    else if(_self.model.ds) {
                        console.log("Nothing in cache, looking in local datastore");
                        _self.emit('load-from-local-ds');
                    }
                    else {
                        console.log("Nothing in cache and no local ds, go directly to backend");
                        _self.emit('execute-backend-op');
                    }

                }).catch(function(err) {
                    reject(err);
                });

            }
            else {

                // Do we have an up to date local DS?
                if(_self.model.ds) {
                    _self.emit('load-from-local-ds');
                }
                else {
                    _self.emit('execute-backend-op');
                }
            }

        }.bind(this));
    }


})(Hermit);