(function (Hermit) {


    Hermit.Operations.update = function () {
        console.log("------------------------------------------");
        console.log("UpdateOperation:execute");
        var defer = Promise.defer(), _self = this;

        // Apply the change to the local datastore
        this.on('update-local-ds', function () {

            this.model.ds.then(function (ds) {

                // Record this change on the device, marking this record as temporary until the operation (hash) is complete
                ds.update(_self.key, _self.data, _.merge(_self.options || {}, {op: _self.hash, temp: true})).then(function (o) {

                    _self.tempResults = o;

                    // Send the operation to the backend
                    _self.emit('sync-backend');

                    return o;
                }).catch(function(err) {
                    defer.reject(err);
                });

            });

        });

        this.once('sync-backend', function () {

            // Execute the operation on the backend
            _self._hermit.getBackend().execute(_self).then(function (result) {

                // If this model supports cache, let's invalidate it for next read
                if (_self.model.cache)
                    _self.emit('invalidate-cache');

                defer.resolve(result);

            }).catch(function (err) {
                // Revert the local ds operation
                _self.emit('revert-local-ds', err);
            });
        });

        this.once('revert-local-ds', function(err) {
            console.log("Reverting %d pending operations because of a failure in the backend", this.tempResults.length);
            this.model.ds.then(function (ds) {

                return Promise.each(_self.tempResults, function(result) {

                    // Remove our operation revision, we've failed.
                    return ds.revert(result._id, result.$revision).then(function() {
                        console.log("Revert was successful");
                        //TODO: Make sure we don't keep track of this operation in our journal
                        _self._hermit.emit("revert-operation", {id:_self.id, data: result, hash: _self.hash, revision: result.$revision, model: _self.model });
                    });

                }).finally(function() {
                    console.log("Reporting the failure of this operation");
                    defer.reject(err);
                });
            });

        });

        // Invalidate any show and list operation cache
        this.once('invalidate-cache', function () {
            var cache = this._hermit.getCache(this.model.cache.key || this.model.key);
            cache.invalidate(Hermit.Operation.makeUniqueKey(this.model.key, 'list'));
            cache.invalidate(Hermit.Operation.makeUniqueKey(this.model.key, 'show', this.key));
        });

        // Apply the update to the local data store
        if (this.model.ds) {
            console.log("Applying the update to the local ds");
            this.emit('update-local-ds');
        }
        else {
            console.log("No DS, send the operation to the backend");
            this.emit('sync-backend');
        }

        return defer.promise;
    }


})(Hermit);