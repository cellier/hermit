(function(Hermit) {


    Hermit.Operations.load = function() {
        console.log("------------------------------------------");
        console.log("LoadOperation:execute");
        var defer = Promise.defer(), _self = this;

        // If we have preloaded data, let's insert them directly in our datastore
        if(this.data) {
            defer.resolve(_self.model.ds.then(function(ds) {
                return ds.reset().then(function() {
                    return ds.insert(_self.data, _.merge(_self.options||{}, { commit: true }));
                });
            }));
        }

        // We need to retrieve the data from our backend
        else {
            console.log("Let's execute our backend operation");

            // Execute the operation on the backend
            defer.resolve(_self._hermit.getBackend().execute(_self, { preflight: true }).then(function(result) {
                console.log("Received %d data page(s) from backend", result._pagination.totalPageCount);

                return _self.model.ds.then(function(ds) {

                    // Reset the model
                    console.log("Reseting the local datastore");
                    return ds.reset().then(function() {
                        var pages = [];

                        if(result._pagination) {
                            var pageSize = result._pagination.requestCount;
                            console.log("Loading %d pages of %d records", result._pagination.totalPageCount, pageSize);
                            for(var i=0; i<result._pagination.totalPageCount; i++) {
                                pages.push({
                                    step: i+1,
                                    total: result._pagination.totalPageCount,
                                    skip: i*pageSize,
                                    size: 10000,
                                    withPageLinks: false
                                });
                            }
                        }
                        else {
                            console.log("Loading a single page (1000 rows max)");
                            // Process a single 1000 page if paging isn't supported
                            pages.push({
                                step: 1,
                                total: 1,
                                skip: 0,
                                size: 10000,
                                withPageLinks: false
                            });
                        }

                        console.log("Processing %d record page(s) with concurrency of 1", pages.length);
                        var pagePromises = Promise.map(pages, function(loadStep) {
                            console.log("Sending backend request to load data record %d to %d",i*pageSize,  i*pageSize + pageSize );

                            // Execute the load step
                            return _self._hermit.getBackend().execute(_self, { query: loadStep }).then(function(result) {
                                //TODO: Uncompress data payload (check result to see if it's compressed first)

                                console.log("Inserting %d rows into the local datastore", result.data.length);
                                return ds.insert(result.data, _.merge(_self.options||{}, { commit: true })).then(function(result) {
                                    console.log("All rows were successfully locally stored", result.data);

                                    var progress = parseInt(((loadStep.step / loadStep.total) * 100).toFixed(0));
                                    console.log("Reporting progress", progress);
                                    _self.emit('progress-operation', _self, {
                                        data: loadStep,
                                        progress: progress,
                                        msg: "Loaded "+_self.model.key+" data records from "+loadStep.skip + " to " + (loadStep.skip + loadStep.size)
                                    });
                                    console.log("Step %d is complete", loadStep.step);
                                    return result;
                                });

                            });

                        }, {concurrency: 10});

                        // Loop through all load pages, with a maximum concurrency of 1 to avoid overloading the network and the database
                        return Promise.all(pagePromises).then(function(processedPages) {
                            console.log("Complete loading model '%s' with %d data pages", _self.model.key, processedPages.length);
                            return processedPages.length;
                        }).catch(function(err) {
                            console.log("Load Error: ", err);
                        });

                    });

                });

            }));
        }

        return defer.promise;
    }


})(Hermit);