(function(Hermit) {
    function applyArgumentsToSpec(model, spec, params) {
        console.log("ApplyArguments", spec, params);

        switch(spec.type) {
            case 'show':
                spec.model = model;
                spec.key = params[0];
                if(spec.subItem) {
                    if(spec.subItem.indexOf(':') === 0) {
                        spec.subItem = params[1];
                        if(params.length > 2)
                            spec.options = params[2];
                        else
                            spec.options = model.sync;
                    }
                    else {
                        if(params.length > 1)
                            spec.options = params[1];
                        else
                            spec.options = model.sync;
                    }
                }
                else {
                    if(params.length > 1)
                        spec.options = params[1];
                    else
                        spec.options = model.sync;
                }
                break;
            case 'list':
                spec.model = model;
                spec.query = params[0];
                if(spec.subItem) {
                    if(spec.subItem.indexOf(':') === 0) {
                        spec.subItem = params[1];
                        if(params.length > 2)
                            spec.options = params[2];
                        else
                            spec.options = model.sync;
                    }
                    else {
                        if(params.length > 1)
                            spec.options = params[1];
                        else
                            spec.options = model.sync;
                    }
                }
                else {
                    if(params.length > 1)
                        spec.options = params[1];
                    else
                        spec.options = model.sync;
                }
                break;
            case 'update':
                spec.model = model;
                spec.key = params[0];
                spec.data = params[1];
                if(spec.subItem) {
                    if(spec.subItem.indexOf(':') === 0) {
                        spec.subItem = params[2];
                        if(params.length > 3)
                            spec.options = params[3];
                        else
                            spec.options = model.sync;
                    }
                    else {
                        if(params.length > 2)
                            spec.options = params[2];
                        else
                            spec.options = model.sync;
                    }
                }
                else {
                    if(params.length > 2)
                        spec.options = params[2];
                    else
                        spec.options = model.sync;
                }
                break;
            case 'remove':
                spec.model = model;
                spec.key = params[0];
                if(spec.subItem) {
                    if(spec.subItem.indexOf(':') === 0) {
                        spec.subItem = params[1];
                        if(params.length > 2)
                            spec.options = params[2];
                        else
                            spec.options = model.sync;
                    }
                    else {
                        if(params.length > 1)
                            spec.options = params[1];
                        else
                            spec.options = model.sync;
                    }
                }
                else {
                    if(params.length > 1)
                        spec.options = params[1];
                    else
                        spec.options = model.sync;
                }
                break;
            case 'insert':
                spec.model = model;
                spec.key = params[0];
                spec.data = params[1];
                if(spec.subItem) {
                    if(spec.subItem.indexOf(':') === 0) {
                        spec.subItem = params[2];
                        if(params.length > 3)
                            spec.options = params[3];
                        else
                            spec.options = model.sync;
                    }
                    else {
                        if(params.length > 2)
                            spec.options = params[2];
                        else
                            spec.options = model.sync;
                    }
                }
                else {
                    if(params.length > 2)
                        spec.options = params[2];
                    else
                        spec.options = model.sync;
                }
                break;
            case 'upsert':
                spec.model = model;
                spec.key = params[0];
                spec.data = params[1];
                if(spec.subItem) {
                    if(spec.subItem.indexOf(':') === 0) {
                        spec.subItem = params[2];
                        if(params.length > 3)
                            spec.options = params[3];
                        else
                            spec.options = model.sync;
                    }
                    else {
                        if(params.length > 2)
                            spec.options = params[2];
                        else
                            spec.options = model.sync;
                    }
                }
                else {
                    if(params.length > 2)
                        spec.options = params[2];
                    else
                        spec.options = model.sync;
                }
                break;
        }

        return spec;
    }

    var Model = function(spec, backend) {
        var _self = this;
        _.merge(this, spec);

        if(!this.key) {
            throw new Hermit.Errors.InvalidSpec();
        }

        if(!this.hermitId)
            this.hermitId = Hermit.getInstance().id;

        var hermit = Hermit.getInstance(this.hermitId);

        // By default, all devices are in their own groups
        if(!this.group) {
            this.group = hermit.group || this.hermitId;
        }

        this.backend = backend;

        // Listen for network in order to trigger sync request when needed (only if we maintain a local datastore)
        if(this.ds) {
            var syncAgent = hermit.getSyncAgent();

            hermit.getNetworkStateManager().on('network-state-changed', function(networkState) {
                console.log("Model:NetworkStateChange", networkState.connected);

                if(networkState.connected) {

                    if(spec.sync && spec.sync.initial) {
                        console.log("We detected that we've been connected to the network. Let's see what we missed!");

                        hermit.getJournal(_self.key).getLastOperation().then(function(lastOp) {
                            console.log("Resolved last operation", lastOp);

                            // Send a sync request to update our datastore with anything we could have missed while we were gone
                            hermit.getConnectedSyncAgent().then(function(agent) {
                                console.log("Performing sync for model %s", spec.key);
                                agent.send([spec.group || hermit.group, hermit.id, spec.key, 'sync'], { lastOp: lastOp, options: spec.sync}).then(function(receipt) {
                                    console.log("Received a sync receipt:", receipt);
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            });

                        });
                    }

                }

            });

            // Register to receive sync data
            syncAgent.register({group: spec.group || hermit.group, model:spec.key, operations:'sync' }, function(operations) {
                console.log("Model '%s' from device '%s' just received %d operation(s) to sync locally", spec.key, _self.hermitId, operations.length);

                // Applying all (not-already applied) operations to our local datastore
                _self.ds.then(function(ds) {
                    console.log("Local DS has been resolved.");

                    // Apply all operations to this data store
                    return Promise.all(Promise.map(operations, function(op) {
                        console.log("Applying operation to model %s", spec.key, op);

                        // Check if we have to confirm or apply this operation
                        if(op.device === hermit.id && op.type !== 'load') {
                            console.log("Confirming operation %s", op.hash);
                            if(Hermit.WaitingOperations[op.hash]) {
                                return _self.confirmOperation(ds, Hermit.WaitingOperations[op.hash].op);
                            }
                            else {
                                return _self.confirmOperation(ds, hermit.wrapOperation(op));
                            }
                        }
                        else {
                            return _self.syncOperation(ds, op);
                        }

                    })).then(function(results) {
                        console.log("Successfully synced %d operation(s) on model %s", results.length, _self.key);
                        if(_self.sync_deferred) {
                            _self.sync_deferred.resolve(results);
                        }
                    }).catch(function(err) {
                        if(_self.sync_deferred) {
                            _self.sync_deferred.reject(err);
                        }
                    });

                }).catch(function(err) {
                    console.log("Error while syncing operations", err);
                    if(_self.sync_deferred) {
                        _self.sync_deferred.reject(err);
                    }
                }).finally(function() {
                    delete _self.sync_deferred;
                });

            });

            syncAgent.register({group: spec.group || hermit.group, model:spec.key, operations:'committed' }, function(op) {
                console.log("Handling committed event for operation %s", op.hash);
                if(Hermit.WaitingOperations[op.hash]) {
                    var existingOp = Hermit.WaitingOperations[op.hash].op;
                    console.log('Operation %s is waiting', op.hash);
                    if(existingOp.options && existingOp.options.waitForCommit) {
                        existingOp.emit('committed', op.data);
                        delete Hermit.WaitingOperations[op.hash];
                    }
                    else
                        console.log("Operation %s doesn't have the waitForCommit option.", op.hash);
                }
                else
                    console.log("No waiting operation %s. Committed event will not be processed", op.hash);
            });

        }

        // Append any custom operations declared in the spec
        if(spec.operations) {
            var opNames = _.keys(spec.operations);
            console.log("Adding %d custom operations", opNames.length);
            _.each(opNames, function(name) {
                _self[name] = (function(opSpec) {
                    opSpec.stereotype = _self.key + "::" + name;
                    return function() {
                        console.log("Executing operation %s with arguments", name, arguments);
                        var result = Hermit.getInstance(_self.hermitId).newOperation(applyArgumentsToSpec(_self, opSpec, arguments)).execute();

                        // Process result if we have additional instructions
                        if(opSpec.result) {
                            console.log("Post processing result for op", opSpec);

                            if(_.isFunction(opSpec.result)) {
                                console.log("Delegating result handling to provided mapping function");
                                return result.then(opSpec.result);
                            }
                            else  if(opSpec.result.field) {
                                console.log("Setting up an extraction logic for operatiaon result");
                                return result.then(function(result) {
                                    console.log("Extracting field %s from result", opSpec.result.field, result);
                                    return result[opSpec.result.field];
                                });
                            }

                        }
                        else
                            return result;
                    }
                })(spec.operations[name]);
            });
        }

    };

    Model.prototype.syncOperation = function(ds, opSpec) {
        var _self = this;
        console.log("Model:syncOperation", opSpec.hash);

        var hermit = Hermit.getInstance(this.hermitId);

        // Transform a simple object into a valid operation
        var op = hermit.getOperationFactory().newOperation(opSpec);

        // Ensure that this operation is not already applied to this device
        return hermit.getJournal(_self.key).getOperation(opSpec.hash).then(function(appliedOp) {
            if(!appliedOp) {

                var invalidateCache = function(result) {

                    // We only invalidate the cache if caching is supported by the target model
                    if(_self.cache) {
                        var cache = hermit.getCache(_self.key);
                        cache.invalidate(_self.key);
                    }
                    return result;
                };

                var promise;

                switch(op.type) {
                    case 'update':
                        promise = ds.update(op.key, op.data, { commit: true }).then(invalidateCache);
                        break;
                    case 'insert':
                        promise = ds.insert(op.data, { commit: true }).then(invalidateCache);
                        break;
                    case 'remove':
                        promise = ds.remove(op.key, { commit: true }).then(invalidateCache);
                        break;
                    case 'upsert':
                        promise = ds.upsert(op.key, op.data, { commit: true }).then(invalidateCache);
                        break;
                    case 'load':
                        console.log("Loading content into model '%s'", op.model.key);
                        return _self.load(op.data, { filter: op.filter }).then(invalidateCache);
                    default:
                        throw new Hermit.Errors.SyncError("Unknown operation type:"+op.type);
                }

                // We send a publish ack back for all operation but load
                if(promise) {
                    promise.then(function() {
                        console.log("Sending publishAck for operation %s", opSpec.hash);
                        hermit.getSyncAgent().send([_self.group || hermit.group, hermit.id, _self.key, 'publish_ack'], [{hash: opSpec.hash, device: hermit.id, id: opSpec.id}]);
                    }).catch(function() {
                        hermit.getSyncAgent().send([_self.group || hermit.group, hermit.id, _self.key, 'publish_error'], {hash: opSpec.hash, device: hermit.id, id: opSpec.id});
                    });
                }

                return promise;
            }
            else {
                console.log("operation %s was already applied on this device", appliedOp.hash);
            }

        });
    };

    Model.prototype.confirmOperation = function(ds, op) {
        console.log("Model:confirmOperation", op.hash);
        return ds.confirm(op.hash).then(function() {

            // Always return our insertion results using an array
            if(!_.isArray(op.data))
                op.data = [op.data];

            console.log("Emitting confirmed event");
            op.emit('confirmed', op.data);
        });
    };

    Model.prototype.manual_sync = function(options) {
        options = options || {};
        console.log("Model:manual_sync (options:", options, ")");
        var _self = this;

        // We accept a single sync operation
        if(_self.sync_deferred) {
            console.log("Already performing a sync operation for model %s. Skipping this one", _self.key);
            return _self.sync_deferred.promise;
        }

        var hermit = Hermit.getInstance(this.hermitId);

        console.log("Syncing model %s", _self.key);
        if(_self.sync) {
            return hermit.getJournal(_self.key).getLastOperation().then(function(lastOp) {
                return hermit.getConnectedSyncAgent().then(function(syncAgent) {
                    console.log("Sending the SYNC request", { lastOp: _.pick(lastOp, "hash", "oplog_ts", "id"), options: _self.sync, model: _self.key });

                    if(!_self.sync_deferred) {
                        _self.sync_deferred = Promise.defer();
                    }

                    // Send the sync request
                    if(options.force) {
                        console.log("Forcing a resync of this device");
                        syncAgent.send([_self.group, hermit.id, _self.key, 'sync'], {options: _self.sync, model: _self.key });
                    }
                    else {
                        console.log("Retrieving operations since %s", lastOp.oplog_ts);
                        syncAgent.send([_self.group, hermit.id, _self.key, 'sync'], { lastOp: _.pick(lastOp, "hash", "oplog_ts", "id"), options: _self.sync, model: _self.key });
                    }

                    _self.sync_deferred.promise.finally(function() {
                        console.log("marking this sync operation as complete");
                        delete _self.sync_deferred;
                    });

                    return _self.sync_deferred.promise;
                });
            });
        }
        else {
            console.log("Sync not supported for model %s. Skipping", _self.key);
        }
    };

    Model.prototype.list = function(query, options) {
        console.log("Model:list");
        return Hermit.getInstance(this.hermitId).newOperation({
            model: this,
            type: 'list',
            query: query,
            options: options || this.sync
        }).execute();
    };

    Model.prototype.show = function(key, options) {
        console.log("Model:show");
        return Hermit.getInstance(this.hermitId).newOperation({
            model: this,
            type: 'show',
            key: key,
            options: options || this.sync
        }).execute();
    };

    Model.prototype.update = function(key, data, options) {
        var hermit = Hermit.getInstance(this.hermitId), _self = this;
        var op = hermit.newOperation({
            model: this,
            type: 'update',
            data: data,
            key: key,
            options: options || this.sync
        });

        return op.execute().then(function(result) {
            console.log("Operation %s was successfully executed", op.id);
            // This operation was successfully applied to the model (including backend), so we record it has our last operation)
            hermit.getJournal(_self.key).recordOperation(op);
            return result;
        });
    };

    Model.prototype.remove = function(key, options) {
        var hermit = Hermit.getInstance(this.hermitId), _self = this;
        var op = hermit.newOperation({
            model: this,
            type: 'remove',
            key: key,
            options: options || this.sync
        });

        return op.execute().then(function(result) {
            console.log("Operation %s was successfully executed", op.id);
            // This operation was successfully applied to the model (including backend), so we record it has our last operation)
            hermit.getJournal(_self.key).recordOperation(op);
            return result;
        });
    };

    Model.prototype.insert = function(data, options) {
        var hermit = Hermit.getInstance(this.hermitId), _self = this;
        var op = hermit.newOperation({
            model: this,
            type: 'insert',
            data: data,
            options: options || this.sync
        });

        return op.execute().then(function(result) {
            console.log("Operation %s was successfully executed", op.id);
            // This operation was successfully applied to the model (including backend), so we record it has our last operation)
            hermit.getJournal(_self.key).recordOperation(op);
            return result;
        });
    };

    Model.prototype.upsert = function(key, data, options) {
        var hermit = Hermit.getInstance(this.hermitId), _self = this;
        var op = hermit.newOperation({
            model: this,
            type: 'upsert',
            data: data,
            key: key,
            options: options || this.sync
        });

        return op.execute().then(function(result) {
            console.log("Operation %s was successfully executed", op.id);
            // This operation was successfully applied to the model (including backend), so we record it has our last operation)
            hermit.getJournal(_self.key).recordOperation(op);

            return result;
        });
    };

    /**
     * Load data into the local datastore. Either preloaded or through a backend call (if network is available).
     *
     * @param preloadedData
     * @param options
     * @returns {*}
     */
    Model.prototype.load = function(preloadedData, options) {
        console.log("Model:load", arguments);
        return Hermit.getInstance(this.hermitId).newOperation({
            model: this,
            type: 'load',
            data: preloadedData,
            options: options || this.sync
        }).execute();
    };

    Hermit.Model = Model;

})(Hermit);
