# Hermit

Hermit is a mobile-friendly javascript library specialized in managing offline data access strategies for html5 applications. Hermit manages a number of model definitions
providing instructions about the kind of offline behavior we expect for each model. Models may be automatically mapped to a RESTful server or through custom mapping functions.

# Getting Started

## Install with bower
Hermit is published as a bower component. You only have to execute the following command: ```bower install hermit```. 

## Install from Git
You may elect to clone the hermit repository and build your own version of Hermit. Just use the following commands : 

```
$ git clone git@bitbucket.org:cellier/hermit.git
$ cd hermit
$ npm install
$ gulp dist
```

This will create the *build/hermit-{version}.min.js file.

## Setting up your application
To use Hermit in your application, just include the hermit script in your HTML file: 

```
<html>
	<head>
		<script src="js/hermit-0.1.0.min.js"></script>
	</head>
</html>
```
This will expose a global Hermit class in your application. 

### Creating your Hermit instance
The default instantiation options should be correct for most simple applications requiring local storage. But if your application has specific requirements, you will have to provide a complete configuration to Hermit when instantiating it. 

This will create a Hermit instance with all default values (see options below for defaults).

```
var hermit = new Hermit();
```
### Hermit options

| Option | Type | Description | Default |
| ------ | ---- | ----------- | ------- |



# API Reference

## Hermit

Hermit is the library entry-point. In order to use Hermit, you need to create an instance and keep it in your application. You can use Hermit to get access to your instance later on,
but you can also keep it in an application-global variable.


| Method | Return Type | Description |
| ------ | ----------- | ----------- |
| **Hermit.getInstance(key)** | Hermit | **static.** Return a Hermit instance. key is an optional unique instance key if your application has multiple instances (rare) |
| model(key) | {Hermit.Model} | Retrieve a previously registered model instance. |



# Contributing to Hermit

Setup Hermit has specified in the getting started section (with Git).

- Build: ```gulp build```
- Work: ```gulp auto```

When running gulp auto, just open your browser to http://localhost:7357. All tests will be run each time you modify a file in the project.

Make sure to create small commits to simplify pull requests processing. Large and complex updates might be rejected.

